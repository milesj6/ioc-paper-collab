function out = plotElasticaCostate(p1, p2)

ts = linspace(0,1,100);
p1s = p1(ts);
p2s = p2(ts);
%x3s = x3(ts);

h = figure
hold on;
plot(ts,p1s(1,:),'bo-');
plot(ts,p1s(2,:),'bo-');
plot(ts,p1s(3,:),'bo-');
plot(ts,p2s(1,:),'g-');
plot(ts,p2s(2,:),'g-');
plot(ts,p2s(3,:),'g-');

end