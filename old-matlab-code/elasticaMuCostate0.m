function u = elasticaMuCostate0(t, x, p0, c)

N = length(t);
u = zeros(1,N);
k = length(c);

for i = 1:N,
    u(i) = - .5*(1.0/(c' * sigma(t(i),k))) * (p0(1)*x(2,i) - p0(2)*x(1,i) + p0(3));
end

end
