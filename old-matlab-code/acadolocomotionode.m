function [ xdot ] = acadolocomotionode( t, x, u, p, w )

global c xf

xdot = zeros(7,1);

xdot(1) = x(4)*cos(x(3)) - x(6)*sin(x(3));
xdot(2) = x(4)*sin(x(3)) + x(6)*cos(x(3));
xdot(3) = x(5);
xdot(4) = u(1);
xdot(5) = u(2);
xdot(6) = u(3);
xdot(7) = c' * phi_locomotion(t, x, u, xf);

end

