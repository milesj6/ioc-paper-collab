function [ out ] = KKT_IOC_elastica3D( t0, tf, c_real, xoptlist, uoptlist, p0_real)

global n m nn M k

k = length(c_real);
% n = 3;
n = 6;
m = 1;
M = length(xoptlist);
nn = k + M*n;

c1_fixed = c_real(1);



A = zeros(nn,nn);
B = zeros(nn, M*n);
B(k+1:end,:) = eye(M*n);

% Riccati solution
tic;
Pf = zeros(nn*nn,1);
options = odeset('RelTol',1e-8,'AbsTol',1e-8);
sol = ode45(@(t,x)rde(t, x, A, B, xoptlist, uoptlist), [tf, t0], Pf, options);
P = @(t)deval(sol,t);

% t_plot = linspace(0,1, 1000);
% plot(P(t_plot)')

% Minimize P(0) w.r.t. z(0) to find cost params and initial costates
P0 = reshape(P(0),nn,nn);
% Aineq = zeros(k,nn); 
% Aineq(1:k,1:k) = -eye(k);
% bineq = zeros(k,1);
LB = 0.01*ones(k,1);
UB = ones(k,1);
Aeq = zeros(1,nn);
Aeq(1) = 1.0;
beq = c1_fixed; % set c(1) := 0.1
% Navid change
options = optimset('Algorithm','interior-point-convex','TolFun',1e-14,'TolX',1e-14,'TolCon',1e-14);
% options = optimset('Algorithm','interior-point','TolFun',1e-14,'TolX',1e-14,'TolCon',1e-14);
[zhat, fval, exitflag, qpoutput, qplambda] = quadprog(P0, [], [], [], Aeq, beq,LB,UB,[],options);

totalTime = toc;
chat = zhat(1:k);
p0 = zhat(k+1:end);  % p0 for all trajectories!


chat 
c_real

% [c_real',p0_real'] * P0 * [c_real; p0_real]
% zhat' * P0 * zhat

out = chat;

%% Compute "predicted" trajectory from learned parameters
% xhist = {};
% uhist = {};
% phist = {};
% xhist2 = {};
% uhist2 = {};
% phist2 = {};
% chist = {};
% feathist = {};
% for i = 1:M
%     x0i = x0list{i};
%     xfi = xflist{i};
%     p0i = p0((i-1)*n + 1:i*n);
%     [xi, ui, pi, ~, ~, ~] = elasticagpopsMain(t0, tf, x0i, xfi, chat, tguess, xguess, uguess);
%     Vi = FeatureExpectations(xi,ui,t0,tf,@phi_elastica,k);
%     costi = chat'*Vi;
%     xhist{i} = {xi};
%     uhist{i} = {ui};
%     phist{i} = {pi};
%     chist{i} = {chat};
%     costhist{i} = {costi};
%     feathist{i} = {Vi};
%     [xi2, ui2, pi2] = getElasticaGivenP0(x0i, p0i, chat);
%     xhist2{i} = {xi2};
%     uhist2{i} = {ui2};
%     phist2{i} = {pi2};
%     
%     fprintf('parm error = %e\n',norm(chat - c));
% %     fprintf('feat error = %e\n',norm(Vi - Voptlist{i}(1:k)));
%     
%     %plotElastica(xoptlist{i}, xi);
% end
% 
% out.totalTime = totalTime;
% out.t0 = t0;
% out.tf = tf;
% out.x0 = x0list;
% out.xf = xflist;
% out.c = c;
% % out.costopt = costoptlist;
% out.xopt = xoptlist;
% out.uopt = uoptlist;
% % out.Vopt = Voptlist;
% out.chist = chist;
% out.xhist = xhist;
% out.uhist = uhist;
% out.phist = phist;
% out.xhist2 = xhist2;
% out.uhist2 = uhist2;
% out.phist2 = phist2;
% out.feathist = feathist;
% out.costhist = costhist;
% out.P0 = P0;
% out.zhat = zhat;
% out.fval = fval;
% out.chat = zhat(1:k);
% out.p0hat = zhat(k+1:end);
% out.exitflag = exitflag;
% out.qpoutput = qpoutput;
% out.qplambda = qplambda;
% 
% outdir = userpath;
% outdir = outdir(1:end-1);
% if timestamp,
%     filename = [outdir filesep timestamp '-kkt-elastica.mat'];
% else
%     filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-kkt-elastica.mat'];
% end
% 
% save(filename, 'out');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Pdot = rde(t, x, A, B, xoptlist, uoptlist)
% Riccati Differential Equation
global nn

P = reshape(x,nn,nn);
Qt = Q(t, xoptlist, uoptlist);
Rt = R();
St = S(t, xoptlist, uoptlist);

Pdot = -A'*P - P*A + (P*B + St)*inv(Rt)*(P*B + St)' - Qt;
Pdot = reshape(Pdot, nn*nn, 1);

end
% 
% function fx = dfdx(t, xt, ut)
% global n
% fx = zeros(n,n);
% fx(1,3) = -sin(xt(3));
% fx(2,3) = cos(xt(3));
% end
% 
% function fu = dfdu(t, xt, ut)
% global n m
% fu = zeros(n,m);
% fu(3) = 1;
% end
% 
% function phix = dphidx(t, xt, ut)
% global k n
% phix = zeros(k,n);
% end
% 
% function phiu = dphidu(t, xt, ut)
% global k
% phiu = sigma(t,k) * 2 * ut;
% end

function Abar = computeAbar(t, xt, ut)
global n m k
Abar = -1*[0 ut(3) -1*ut(2) 0 0 0;
           -1*ut(3) 0 ut(1) 0 0 1;
           ut(2) -1*ut(1) 0 0 -1 0;
           0 0 0 0 ut(3) -1*ut(2);
           0 0 0 -1*ut(3) 0 ut(1);
           0 0 0 ut(2) -1*ut(1) 0];

end
% 
% function Bbar = computeBbar(t, xt, ut)
% global n m
% Bbar = zeros(n+m,n);
% Bbar(1:n,:) = dfdx(t, xt, ut)';
% Bbar(n+1:n+m,:) = dfdu(t, xt, ut)';
% end
% 
% function Cbar = computeCbar()
% global n m
% Cbar = zeros(n+m,n);
% Cbar(1:n,:) = eye(n);
% end

function F = computeF(t, xoptlist, uoptlist)
global n m k M
F = zeros(M*(n+k), k + M*n);
for i = 1:M
    xt = xoptlist{i}(t);
    ut = uoptlist{i}(t);
    i1 = (i-1)*(n+k)+1;   % row start index
    i2 = (i-1)*(n+k)+k;       % row end   index
    i3 = i2 + 1;   % row start index
    i4 = i2 + n;   % row end index
    j1 = k + 1 + (i-1)*n; % column start index
    j2 = 2*k + (i-1)*n;   % column end   index
    j3 = k + (i)*n;   % column end   index
    F(i1:i2, 1:k) = diag([ut(1);ut(2);ut(3)]);
    F(i1:i2, j1:j2) = -1*eye(k); 
    F(i3:i4, j1:j3) = computeAbar(t, xt, ut);
end
end

function F = computeFSingle(t, xoptlist, uoptlist)
global n m k M
% F = zeros(M*(n+m), k + M*n);
xt = xoptlist{1}(t);
ut = uoptlist{1}(t);
Mat1 = -1*[0 0 0 0 ut(3) -1*ut(2) 0 0 0;
           0 0 0 -1*ut(3) 0 ut(1) 0 0 1;
           0 0 0 ut(2) -1*ut(1) 0 0 -1 0;
           0 0 0 0 0 0 0 ut(3) -1*ut(2);
           0 0 0 0 0 0 -1*ut(3) 0 ut(1);
           0 0 0 0 0 0 ut(2) -1*ut(1) 0];

Mat2 = [ut(1) 0 0 -1 0 0 0 0 0;
        0 ut(2) 0 0 -1 0 0 0 0;
        0 0 ut(3) 0 0 -1 0 0 0];
    
F = [Mat1; Mat2];

% This is just to make is consistent with previous dimensions
% F = [F ; zeros(3, 9)];

end


% 
% function G = computeG()
% global n m M
% G = zeros(M*(n+m), M*n);
% for i = 1:M
%     i1 = (i-1)*(n+m)+1;  % row start index
%     i2 = (i) * (n+m);    % row   end index
%     j1 = (i-1)*n + 1;    % col start index
%     j2 = (i)*n;          % col   end index
%     G(i1:i2, j1:j2) = computeCbar();
% end
% end

function G = computeG()
global n k M
G = zeros(M*(n+k), M*n);

for i = 1:M
    i1 = (i-1)*(n+k)+1;   % row start index
    i2 = (i-1)*(n+k)+k;       % row end   index
    i3 = i2 + 1;   % row start index
    i4 = i2 + n;   % row end index
    
    j1 = (i-1)*n + 1;
    j2 = i*n;
    
    G(i3:i4, j1:j2) = eye(n);
end

end



function G = computeGSingle()
global n m M
% G = zeros(M*(n+m), M*n);

G = [ eye(n) ; zeros(3, n)];

% This is just to make is consistent with previous dimensions
% G = [G ; zeros(3, n)];

end

function Qout = Q(t, xoptlist, uoptlist)
Ft = computeF(t, xoptlist, uoptlist);
Qout = Ft'*Ft;
end

function Rout = R()
Gt = computeG();
Rout = Gt'*Gt;
end

function Sout = S(t, xoptlist, uoptlist)
Ft = computeF(t, xoptlist, uoptlist);
Gt = computeG();
Sout = Ft'*Gt;
end

