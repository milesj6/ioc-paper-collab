function [xopt, uopt, popt, xoptp, uoptp, poptp, tfopt] = locomotiongpopsMain(t0, tf, x0, xfin, cin, tguess, xguess, uguess)

clear global setup limits guess;

%%%%%%%%%% set up global variables %%%%%%%%%
global c k xf
k = length(cin);  % number of basis functions = n + m
c = cin;
xf = xfin;
%%%%%%%%%% end global variables %%%%%%%%%

disp('Solve forward optimal control problem...');
disp('');

n = 6; % state vector dimension
m = 3; % input vector dimension

xmin = [-10 -10 -pi -10 -10 -10]';
xmax = [ 10  10  pi  10  10  10]';
if any(isnan(xf)),
    xfmin = xmin;
    xfmax = xmax;
    xf = [0,0,0,0,0,0]';
else
    xfmin = xf;
    xfmax = xf;
end
umin = [-50 -50 -50]';
umax = [ 50  50  50]';

if isnan(tf),
    tfmin = 3;
    tfmax = 7;
    tf = (tfmax - tfmin)/2 + tfmin;
else
    tfmin = tf;
    tfmax = tf;
end

iphase = 1;  % only one "phase"

%%%%%%%%% Set up limits %%%%%%%%%%%%%%%
%limits(iphase).meshPoints = -1:0.2:1;
%limits(iphase).nodesPerInterval = 10*ones(1,length(limits(iphase).meshPoints)-1);
limits(iphase).time.min = [t0 tfmin];
limits(iphase).time.max = [t0 tfmax];
limits(iphase).state.min = [x0 xmin xfmin];
limits(iphase).state.max = [x0 xmax xfmax];
limits(iphase).control.min = umin;
limits(iphase).control.max = umax;
limits(iphase).parameter.min = [];
limits(iphase).parameter.max = [];
limits(iphase).path.min = [];
limits(iphase).path.max = [];
limits(iphase).event.min = [];
limits(iphase).event.max = [];

%%%%%%%% Set up guesses %%%%%%%%%%%%%%
%tguess = linspace(0,1,50);
%xguess_samp = xguess(tguess);
%for i=1:length(tguess)
%    uguess_samp(i) = uguess(tguess(i));
%end
%uguess_samp = uguess(tguess);
guess(iphase).time = tguess;
guess(iphase).state = xguess;  % N x n matrix of state vectors row by row
guess(iphase).control = uguess; % N x m matrix of control vectors
%guess(iphase).time = [t0; tf]; % column vector of times
%guess(iphase).state = [x0'; xf'];  % N x n matrix of state vectors row by row
%guess(iphase).control = [[0 0 0]; [0 0 0]]; % N x m matrix of control vectors
%guess(iphase).time = tguess'; %[t0; tf];
%guess(iphase).state = xguess_samp'; %[x0'; xf'];
%guess(iphase).control = uguess_samp'; %[0; 0];
guess(iphase).parameter = [];

setup.name = 'locomotion';
setup.funcs.cost = 'locomotiongpopsCost';
setup.funcs.dae = 'locomotiongpopsDAE';
setup.printoff = 0;
setup.limits = limits;
setup.derivatives = 'finite-difference';    %'finite-difference';
%setup.derivatives = 'automatic';
setup.checkDerivatives = 0;
setup.guess = guess;
setup.linkages = [];
setup.autoscale = 'off';
setup.tolerances = [1e-6,1e-6];
setup.mesh.tolerance = 1e-3;
%setup.mesh.iteration = 0;
setup.mesh.iteration = 5;
setup.mesh.nodesPerInterval.min = 10;
setup.mesh.nodesPerInterval.max = 100;

[output, gpopsHistory] = gpops(setup);
solution = output.solution;
solutionPlot = output.solutionPlot;

topt = solutionPlot.time;
xoptsamp = solutionPlot.state;
uoptsamp = solutionPlot.control;
poptsamp = solutionPlot.costate;

% interpolate x, and u to get obtain state and input as functions of time
xtmp = csaps(topt, xoptsamp', 1);
utmp = csaps(topt, uoptsamp', 1);
ptmp = csaps(topt, poptsamp', 1);
xopt = @(t) fnval(xtmp,t);
uopt = @(t) fnval(utmp,t);
popt = @(t) fnval(ptmp,t);

xtmpprime = fnder(xtmp);
utmpprime = fnder(utmp);
ptmpprime = fnder(ptmp);
xoptp = @(t) fnval(xtmpprime,t);
uoptp = @(t) fnval(utmpprime,t);
poptp = @(t) fnval(ptmpprime,t);

tfopt = solutionPlot.time(end);
disp('done...');
end