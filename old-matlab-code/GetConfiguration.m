function conf = GetConfiguration(a)

% Basis for the Lie algebra
X{1} = [0 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 0];
X{2} = [0 0 1 0; 0 0 0 0; -1 0 0 0; 0 0 0 0];
X{3} = [0 -1 0 0; 1 0 0 0; 0 0 0 0; 0 0 0 0];
X{4} = [0 0 0 1; 0 0 0 0; 0 0 0 0; 0 0 0 0];
X{5} = [0 0 0 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
X{6} = [0 0 0 0; 0 0 0 0; 0 0 0 1; 0 0 0 0];

% Tolerance when checking for the event that det(J) has a zero crossing
tolStability = 1e-8;

% Parameters that govern collision checking...
tolCollision = 2^-4;
wCollision = 0.75*(2^(-5));
sCollision = 5*wCollision;

% Get the configuration
conf = struct;
musol = GetMU(a);
MJsol = GetMJ(musol,tolStability);
nt = 1000;
t = linspace(0,1,nt);
MJ = deval(MJsol,t);
J = MJ(37:72,:);
detJ = zeros(1,nt);
for i=1:nt
    detJ(i) = det(reshape(J(:,i),6,6));
end
conf.a = a;
conf.musol = musol;
conf.MJsol = MJsol;
conf.nt = nt;
conf.t = t;
conf.J = J;
conf.detJ = detJ;
conf.qsol = GetQ(conf.musol,X);
conf.q = deval(conf.qsol,t);
conf.qpos = conf.q(13:15,:);

% Check for instability
[res,tconj] = IsUnstable(MJsol);
conf.stable = ~res;

% Check for collision
[res,B1tmp,B2tmp] = InCollision(conf,tolCollision,wCollision,sCollision);
conf.collisionfree = ~res;

% Check for infeasibility
conf.infeasible = ~(conf.stable&&conf.collisionfree);



function sol = GetMU(a)
sol = ode45(@(t,x) fMU(t,x),[0,1],a,odeset('reltol',1e-4,'abstol',1e-8));

function mudot = fMU(t,mu)
mudot = [0;
         mu(6);
         -mu(5);
         mu(3)*mu(5)-mu(2)*mu(6);
         -mu(3)*mu(4)+mu(1)*mu(6);
         mu(2)*mu(4)-mu(1)*mu(5)];

function sol = GetQ(musol,X)
sol = ode45(@(t,x) fQ(t,x,musol,X),[0,1],reshape(eye(4),16,1),odeset('reltol',1e-4,'abstol',1e-8));

function qdot = fQ(t,q,musol,X)
q = reshape(q,4,4);
mu = deval(musol,t);
qdot = q*(mu(1)*X{1}+mu(2)*X{2}+mu(3)*X{3}+X{4});
qdot = reshape(qdot,16,1);

function sol = GetMJ(musol,tol)
sol = ode45(@(t,x) fMJ(t,x,musol),[0,1],reshape([eye(6) zeros(6)],72,1),odeset('reltol',1e-4,'abstol',1e-8,'events',@(t,x) conjMJ(t,x,tol)));

function MJdot = fMJ(t,MJ,musol)
mu = deval(musol,t);
F = [0 0 0 0 0 0;
     0 0 0 0 0 1;
     0 0 0 0 -1 0;
     0 -mu(6) mu(5) 0 mu(3) -mu(2);
     mu(6) 0 -mu(4) -mu(3) 0 mu(1);
     -mu(5) mu(4) 0 mu(2) -mu(1) 0];
G = diag([1 1 1 0 0 0]);
H = [0 mu(3) -mu(2) 0 0 0;
     -mu(3) 0 mu(1) 0 0 0;
     mu(2) -mu(1) 0 0 0 0;
     0 0 0 0 mu(3) -mu(2);
     0 0 1 -mu(3) 0 mu(1);
     0 -1 0 mu(2) -mu(1) 0];
MJ = reshape(MJ,6,12);
M = MJ(:,1:6);
J = MJ(:,7:12);
MJdot = [F*M G*M+H*J];
MJdot = reshape(MJdot,72,1);

function [res,tconj] = IsUnstable(solz)
ithreshold = find(solz.ie==2,1,'first');
res = 0;
tconj = [];
if (~isempty(ithreshold))
    iconj = find(solz.ie==1);
    iconj = iconj(iconj>ithreshold);
    if (~isempty(iconj))
        iconj = iconj(1);
        tconj = solz.xe(iconj);
        res = 1;
    end
end

function [value,isterminal,direction] = conjMJ(t,x,tol)
detJ = det(reshape(x(37:72,1),6,6));
value = [detJ; abs(detJ)-tol];
isterminal = [0; 0];
direction = [0; 0];

