function [ cineq, ceq ] = maxmargin_nonlincon( c )


cineq = norm(c) - 10;  % i.e. norm(c) <= 10.
ceq = [];  % i.e. no nonlinear equality constraints.

end

