function output = plotData()

close all

global save perturbation
save = 1;
perturbation = 1;

currentFolder = pwd;
%ignore = '/data/ae/shares/MotionLab/miles/ioc-comparison-data/20120423/';
ignore = '/Users/miles/Documents/MATLAB/';
cd(ignore);

%data_strings = {'lqr','unicycle','elastica'};
%data_strings = {'unicycle','elastica'};
data_strings = {'lqr'};
data_strings2 = {'direct', 'max-margin','max-margin3','kkt'};
%data_strings2 = {'max-margin3'};
%data_strings2 = {'kkt'};

colors = {'b','g','r','m'};
%symbols = {'o','s','d','^'};
symbols = {'','','',''};

output = {};
for i = 1:length(data_strings)
    for j = 1:length(data_strings2)
        fnames = dir(['*',data_strings2{j},'-',data_strings{i},'*.mat']);
        if length(fnames) >= 1
            output{i,j} = average_run_times(fnames, data_strings{i}, data_strings2{j});
            %average_run_times(fnames, data_strings{i}, data_strings2{j});
        end
        if j == 4
            plot_all_xopt(fnames,data_strings{i});
        end
    end
end

%% Plot perturbation results for each system, all methods
if perturbation

    for i = 1:length(data_strings)
        h = figure();
        hold on;
        if strcmp(data_strings{i},'lqr')
            epsilon_mag = 10.^linspace(-5,-2,20)';
        elseif strcmp(data_strings{i},'unicycle')
            epsilon_mag = 10.^linspace(-5,-1,20)';
        elseif strcmp(data_strings{i},'elastica')
            epsilon_mag = 10.^linspace(-5,0,20)';
        end
        for j = 1:length(data_strings2)
            loglog(epsilon_mag, output{i,j}.finalfeatexperrors, strcat(colors{j},symbols{j},'-'),'Linewidth',4);
        end
        set(gca,'LineWidth',2,'FontSize',20);
        set(gca,'XScale','log');
        set(gca,'YScale','log');
        %xlabel('perturbation magnitude','FontSize',20);
        %ylabel('feat errror','FontSize',20);
        %title('feature vector error vs perturbation magnitude');
        %legend('Mombaur','Abbeel','Ratliff','New','Location','Best');
        if save
            filename = [data_strings{i},'-all-perturb-featerror.eps'];
            saveas(h, filename, 'epsc2');
        end
        close(h);
        h = figure();
        hold on;
        if strcmp(data_strings{i},'lqr')
            epsilon_mag = 10.^linspace(-5,-2,20)';
        elseif strcmp(data_strings{i},'unicycle')
            epsilon_mag = 10.^linspace(-5,-1,20)';
        elseif strcmp(data_strings{i},'elastica')
            epsilon_mag = 10.^linspace(-5,0,20)';
        end
        for j = 1:length(data_strings2)
            loglog(epsilon_mag, output{i,j}.finaltrajerrors, strcat(colors{j},symbols{j},'-'),'Linewidth',4);
        end
        set(gca,'LineWidth',2,'FontSize',20);
        set(gca,'XScale','log');
        set(gca,'YScale','log');
        %xlabel('perturbation magnitude','FontSize',20);
        %ylabel('traj errror','FontSize',20);
        %title('trajectory error vs perturbation magnitude');
        %legend('Mombaur','Abbeel','Ratliff','New','Location','Best');
        if save
            filename = [data_strings{i},'-all-perturb-trajerror.eps'];
            saveas(h, filename, 'epsc2');
        end
        close(h);
    end
end

cd(currentFolder);  % cd back to starting folder.
end % end function.



%% Helper Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function output = average_run_times(fnames, data_string, data_string2)
global save perturbation
close all
N = size(fnames,1);
[pathstr, name, ext] = fileparts(fnames(1).name);

fprintf('system: %s, method: %s\n\n',data_string,data_string2);

total_times = zeros(N,1);
iterations = zeros(N,1);
finalparmerrors = zeros(N,1);
finalcosterrors = zeros(N,1);
finalfeatexperrors = zeros(N,1);
finaltrajerrors = zeros(N,1);

for i = 1:N
    filename = fnames(i).name
    datatmp = load(filename);
    data = datatmp.out;
    
    %% Mombaur's method
    if strcmp(data_string2,'direct'),
        total_times(i) = data.totaTime;  % total cpu time
        iterations(i) = length(data.xhist);  % number of iterations
        % evolution of parameter error
        error = [];  
        for j = 1:length(data.chist)
           error(j) = norm(data.chist{j} - data.c)/norm(data.c);
        end
        squared_parm_error{i} = error;
        % evolution of total cost error
        error = [];
        for j = 1:length(data.costhist)
            %error(j) = abs(data.costhist{j} - data.costopt)/abs(data.costopt);
            if strcmp(data_string,'unicycle')
                tmpk = length(data.c);
                tmpfeat = FeatureExpectations(data.xhist{j}, data.uhist{j}, data.t0, data.tf, @phi_unicycle, tmpk);
                tmpcost = data.c'*tmpfeat;
            else
                tmpcost = data.c'*data.feathist{j};
            end
            error(j) = abs(tmpcost - data.costopt)/abs(data.costopt);
        end
        squared_cost_error{i} = error;
        % evolution of feature expectation error
        error = [];
        for j = 1:length(data.feathist)
            % recompute feature vector b/c of bug in Direct unicycle code
            if strcmp(data_string,'unicycle')
                tmpk = length(data.c);
                tmpfeat = FeatureExpectations(data.xhist{j}, data.uhist{j}, data.t0, data.tf, @phi_unicycle, tmpk);
                error(j) = norm(tmpfeat - data.Vopt)/norm(data.Vopt);
            else
                error(j) = norm(data.feathist{j} - data.Vopt)/norm(data.Vopt);
            end
        end
        featexperrors{i} = error;
        % evolution of trajectory error
        n = length(data.x0);
        m = length(data.uopt(0));
        T = linspace(data.t0, data.tf, 50);
        xuopt = [reshape(data.xopt(T)', n*length(T), 1) ; reshape(data.uopt(T)', m*length(T),1)];
        trajerrors{i} = [data.objectivehist{:}]/(norm(xuopt)^2);
        % final parameter error
        finalparmerrors(i) = norm(data.c - [0.1;data.chat])/norm(data.c);
        % final total cost error
        finalcosterrors(i) = squared_cost_error{i}(end);
        % Final feature expectation error
        finalfeatexperrors(i) = featexperrors{i}(end);
        % Final trajectory sum squared error
        finaltrajerrors(i) = trajerrors{i}(end);
    end
    %% Abbeel's method
    if strcmp(data_string2,'max-margin'),
        total_times(i) = data.totalTime;  % total cpu time
        iterations(i) = length(data.xhist);  % number of iterations
        % evolution of parameter error
        error = [];
        for j = 1:length(data.chist),
            error(j) = norm(data.chist{j} - data.c)/norm(data.c);
        end
        squared_parm_error{i} = error;
        % evolution of total cost error
        error = [];
        for j = 1:length(data.costhist)
            %error(j) = abs(data.costhist{j} - data.costopt)/abs(data.costopt);
            tmpcost = data.c'*data.feathist{j};
            error(j) = abs(tmpcost - data.costopt)/abs(data.costopt);
        end
        squared_cost_error{i} = error;
        % evolution of feature expectation error
        error = [];
        for j = 1:length(data.feathist)
            error(j) = norm(data.feathist{j} - data.Vopt)/norm(data.Vopt);
        end
        featexperrors{i} = error;
        % evolution of margin
        margins{i} = abs([data.whist{2:end}]);
        % final parameter error
        [bestfeaterr, bestind] = min(featexperrors{i});
        finalparmerrors(i) = norm(data.c - data.chist{bestind})/norm(data.c);
        fprintf('abbeel: best c = \n');
        data.chist{bestind}*10
        % final total cost error
        finalcosterrors(i) = squared_cost_error{i}(bestind);
        % Final feature expectation error
        finalfeatexperrors(i) = featexperrors{i}(bestind);
        % Final trajectory sum squared error
        T = linspace(data.t0, data.tf, 50);
        n = length(data.x0);
        m = length(data.uopt(0));
        xuopt = [reshape(data.xopt(T)', n*length(T), 1) ; reshape(data.uopt(T)', m*length(T),1)]; % stack of all x's and u's
        xuf = [reshape(data.xhist{bestind}(T)', n*length(T), 1) ; reshape(data.uhist{bestind}(T)', m*length(T),1)];
        finaltrajerrors(i) = sum((xuopt - xuf).^2)/(norm(xuopt)^2);
    end
    %% Ratliff's method
    if strcmp(data_string2,'max-margin3'),
        total_times(i) = data.totalTime;  % total cpu time
        iterations(i) = length(data.xhist);  % number of iterations
        % evolution of parameter error
        error = [];  
        for j = 1:length(data.chist)
           error(j) = norm(data.chist{j} - data.c)/norm(data.c);
        end
        squared_parm_error{i} = error;
        % evolution of total cost error
        error = [];
        for j = 1:length(data.costhist)
            %error(j) = abs(data.costhist{j} - data.costopt)/abs(data.costopt);
            tmpcost = data.c'*data.feathist{j};
            error(j) = abs(tmpcost - data.costopt)/abs(data.costopt);
        end
        squared_cost_error{i} = error;
        % evolution of feature expectation error
        error = [];
        for j = 1:length(data.feathist)
            error(j) = norm(data.feathist{j} - data.Vopt)/norm(data.Vopt);
        end
        featexperrors{i} = error;
        % final parameter error
        finalparmerrors(i) = squared_parm_error{i}(end);
        % final total cost error
        finalcosterrors(i) = squared_cost_error{i}(end);
        % Final feature expectation error
        finalfeatexperrors(i) = featexperrors{i}(end);
        % Final trajectory sum squared error
        T = linspace(data.t0, data.tf, 50);
        n = length(data.x0);
        m = length(data.uopt(0));
        xuopt = [reshape(data.xopt(T)', n*length(T), 1) ; reshape(data.uopt(T)', m*length(T),1)]; % stack of all x's and u's
        xuf = [reshape(data.xhist{end}(T)', n*length(T), 1) ; reshape(data.uhist{end}(T)', m*length(T),1)];
        finaltrajerrors(i) = sum((xuopt - xuf).^2)/(norm(xuopt)^2);
    end
    %% New (KKT) method
    if strcmp(data_string2,'kkt'),
        k = length(data.chat);
        total_times(i) = data.totalTime;  % total cpu time
        iterations(i) = 0;
        % evolution of parameter error
        %fprintf('c = \n');
        %disp(data.c');
        %fprintf('chat = \n');
        %disp(data.chist{1}{1}');
        error = [];  
        for j = 1:length(data.chist)
            %error(j) = norm(data.chist{j}{1} - data.c)/norm(data.c);
            error(j) = norm(data.chist{j} - data.c)/norm(data.c);
        end
        squared_parm_error{i} = error;
        % evolution of total cost error
        error = [];
        for j = 1:length(data.costhist)
            %error(j) = abs(data.costhist{j} - data.costopt{j})/abs(data.costopt{j});
            %tmpcost = data.c'*data.feathist{j}{1};
            tmpcost = data.c'*data.feathist{j};
            error(j) = abs(tmpcost - data.costopt{j})/abs(data.costopt{j});
        end
        squared_cost_error{i} = error;
        % feature expectation error
        error = [];
        for j = 1:length(data.feathist)
            %error(j) = norm(data.feathist{j}{1} - data.Vopt{j}(1:k))/norm(data.Vopt{j}(1:k));
            error(j) = norm(data.feathist{j} - data.Vopt{j}(1:k))/norm(data.Vopt{j}(1:k));
        end
        featexperrors{i} = error;
        % final parameter error
        finalparmerrors(i) = norm(data.c(1:k) - data.chat)/norm(data.c(1:k));
        % final total cost error
        finalcosterrors(i) = squared_cost_error{i}(end);
        % Final feature expectation error
        finalfeatexperrors(i) = featexperrors{i}(end);
        % Final trajectory sum squared error
        T = linspace(data.t0, data.tf, 50);
        n = length(data.x0{1});
        m = length(data.uopt{1}(0));
        xuopt = [reshape(data.xopt{1}(T)', n*length(T), 1) ; reshape(data.uopt{1}(T)', m*length(T),1)]; % stack of all x's and u's
        %xuf = [reshape(data.xhist{end}{1}(T)', n*length(T), 1) ; reshape(data.uhist{end}{1}(T)', m*length(T),1)];
        xuf = [reshape(data.xhist{end}(T)', n*length(T), 1) ; reshape(data.uhist{end}(T)', m*length(T),1)];
        finaltrajerrors(i) = sum((xuopt - xuf).^2)/(norm(xuopt)^2);
    end

        disp('learned c')
        data.chist{end}*10
    
    if strcmp(data_string2,'max-margin') || strcmp(data_string2,'max-margin3'),
        %plotMaxMarginRun(filename, data, squared_parm_error{i}, squared_cost_error{i}, featexperrors{i});
    end
    
    if strcmp(data_string2,'direct')
        %plotDirectRun(filename, data, squared_parm_error{i}, squared_cost_error{i}, featexperrors{i}, trajerrors{i});
    end

    if strcmp(data_string2,'kkt')
        %plotKKTRun(filename, data);
    end
    
end % end for
ave_time = mean(total_times);
std_time = std(total_times);
ave_iterations = mean(iterations);
std_iterations = std(iterations);
ave_finalparmerrors = mean(finalparmerrors);
std_finalparmerrors = std(finalparmerrors);
ave_finalcosterrors = mean(finalcosterrors);
std_finalcosterrors = std(finalcosterrors);
ave_finalfeatexperrors = mean(finalfeatexperrors);
std_finalfeatexperrors = std(finalfeatexperrors);
ave_finaltrajerrors = mean(finaltrajerrors);
std_finaltrajerrors = std(finaltrajerrors);
fprintf('average time: %.1f\n',ave_time);
fprintf('std dev time: %.1f\n',std_time);
fprintf('ave num of iterations: %.0f\n', ave_iterations);
fprintf('std dev of iterations: %.0f\n', std_iterations);
fprintf('ave final parm error: %.3e\n', ave_finalparmerrors);
fprintf('std final parm error: %.3e\n', std_finalparmerrors);
fprintf('ave final cost error: %.3e\n', ave_finalcosterrors);
fprintf('std final cost error: %.3e\n', std_finalcosterrors);
fprintf('ave final feat exp error: %.3e\n', ave_finalfeatexperrors);
fprintf('std final feat exp error: %.3e\n', std_finalfeatexperrors);
fprintf('ave final traj error: %.3e\n', ave_finaltrajerrors);
fprintf('std final traj error: %.3e\n', std_finaltrajerrors);
try
output.squared_parm_error = squared_parm_error;
h = figure();
hold on
maxi = 0;
for i = 1:N
    semilogy(squared_parm_error{i},'-','LineWidth',2,'Color',[0.063,0.294,0.663]);
    if length(squared_parm_error{i}) > maxi
        maxi = length(squared_parm_error{i});
    end
end
set(gca,'YScale','log','LineWidth',2,'FontSize',20);
xlabel('iterations','FontSize',20);
ylabel('parameter error','FontSize',20);
xlim([1,maxi]);
if save
    filename = [data_string2,'-',data_string,'-all-parmerror.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)
catch
end

try
output.squared_cost_error = squared_cost_error;
h = figure();
hold on
maxi = 0;
for i = 1:N
    semilogy(squared_cost_error{i},'-','LineWidth',2,'Color',[0.063,0.294,0.663]);
    if length(squared_cost_error{i}) > maxi
        maxi = length(squared_cost_error{i});
    end
end
set(gca,'YScale','log','LineWidth',2,'FontSize',20);
xlabel('iterations','FontSize',20);
ylabel('cost error','FontSize',20);
xlim([1,maxi]);
if save
    filename = [data_string2,'-',data_string,'-all-costerror.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)
catch
end

try
output.margins = margins;
h = figure();
hold on
maxi = 0;
for i = 1:N
    semilogy(margins{i},'-','LineWidth',2,'Color',[0.063,0.294,0.663]);
    if length(margins{i}) > maxi
        maxi = length(margins{i});
    end
end
set(gca,'YScale','log','LineWidth',2,'FontSize',20);
ylim([10^-5, 10^4]);
xlim([1,maxi]);
xlabel('iterations','FontSize',20);
ylabel('margin','FontSize',20);
if save
    filename = [data_string2,'-',data_string,'-all-margins.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)
catch
end

try
output.featexperrors = featexperrors;
h = figure();
hold on
maxi = 0;
for i = 1:N
    semilogy(featexperrors{i},'-','LineWidth',2,'Color',[0.063,0.294,0.663]);
    if length(featexperrors{i}) > maxi
        maxi = length(featexperrors{i});
    end
end
set(gca,'YScale','log','LineWidth',2,'FontSize',20);
ylim([10^-3, 10^1]);
xlim([1,maxi]);
xlabel('iterations','FontSize',20);
ylabel('feature error','FontSize',20);
if save
    filename = [data_string2,'-',data_string,'-all-featerrors.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)
catch
end

try
output.trajerrors = trajerrors;
h = figure();
hold on
maxi = 0;
switch data_string2
    case 'direct'
        ylimits = [10^-6, 10^1];
    case 'max-margin3'
        ylimtis = [10^-3, 10^1];
end
for i = 1:N
    semilogy(trajerrors{i},'-','LineWidth',2,'Color',[0.063,0.294,0.663]);
    if length(trajerrors{i}) > maxi
        maxi = length(trajerrors{i});
    end
end
set(gca,'YScale','log','LineWidth',2,'FontSize',20);
ylim(ylimits);
xlim([1,maxi]);
xlabel('iterations','FontSize',20);
ylabel('trajectory error','FontSize',20);
if save
    filename = [data_string2,'-',data_string,'-all-trajerrors.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)
catch
end

%% Perturbation plots
if perturbation
    if strcmp(data_string,'lqr')
        epsilon_mag = 10.^linspace(-5,-2,20)';
    elseif strcmp(data_string,'unicycle')
        epsilon_mag = 10.^linspace(-5,-1,20)';
    elseif strcmp(data_string,'elastica')
        epsilon_mag = 10.^linspace(-5,0,20)';
    end
    h = figure();
    size(finalparmerrors)
    size(epsilon_mag)
    loglog(epsilon_mag, finalparmerrors, 'bo-');
    xlabel('perturbation magnitude');
    ylabel('parm errror');
    title('parameter error vs perturbation magnitude');
    if save
        filename = [data_string2,'-',data_string,'-perturb-parmerror.eps'];
        saveas(h, filename, 'epsc2');
    end
    close(h);
    
    h = figure();
    loglog(epsilon_mag, finalcosterrors, 'bo-');
    xlabel('perturbation magnitude');
    ylabel('cost errror');
    title('cost error vs perturbation magnitude');
    if save
        filename = [data_string2,'-',data_string,'-perturb-costerror.eps'];
        saveas(h, filename, 'epsc2');
    end
    close(h);
    
    h = figure();
    loglog(epsilon_mag, finalfeatexperrors, 'bo-');
    xlabel('perturbation magnitude');
    ylabel('feat errror');
    title('feature vector error vs perturbation magnitude');
    if save
        filename = [data_string2,'-',data_string,'-perturb-featerror.eps'];
        saveas(h, filename, 'epsc2');
    end
    close(h);
    
    h = figure();
    loglog(epsilon_mag, finaltrajerrors, 'bo-');
    xlabel('perturbation magnitude');
    ylabel('traj errror');
    title('trajectory error vs perturbation magnitude');
    if save
        filename = [data_string2,'-',data_string,'-perturb-trajerror.eps'];
        saveas(h, filename, 'epsc2');
    end
    close(h);
end

output.finalfeatexperrors = finalfeatexperrors;
output.finaltrajerrors = finaltrajerrors;
output.ave_time = ave_time;
output.ave_iterations = ave_iterations;
output.ave_finalparmerrors = ave_finalparmerrors;
output.ave_finalcosterrors = ave_finalcosterrors;
output.ave_finalfeatexperrors = ave_finalfeatexperrors;
output.ave_finaltrajerrors = ave_finaltrajerrors;


end % end function







function plot_all_xopt(fnames, titlestring)
global save

if length(fnames) == 0
    return
end

N = size(fnames,1);
[pathstr, name, ext] = fileparts(fnames(1).name);
        
h = figure();
hold on
axis equal
for i = 1:N
    filename = fnames(i).name;
    datatmp = load(filename);
    data = datatmp.out;
    t = linspace(data.t0,data.tf,100);
    xsamp = data.xopt{1}(t);
    usamp = data.uopt{1}(t);
    plot(xsamp(1,:), xsamp(2,:), 'b-','Linewidth',2);
end
xlabel('x1');
ylabel('x2');
title('All Trajectories, x1 x2 plane');
if save
    filename = [titlestring,'-all-opt-traj.eps'];
    saveas(h, filename, 'epsc2');
end
close(h)

end



%% 
function plotMaxMarginRun(filename, data, squared_parm_error, squared_cost_error, featexperrors)
    close all
    [pathstr, name, ext] = fileparts(filename);
    
    t = linspace(data.t0, data.tf, 100); %linspace(0,1,100);
    xsamp = data.xopt(t);
    usamp = data.uopt(t);

    h = figure();
    hold on
    NN = length(data.xhist);
    for i=1:NN,
        b = i/NN;
        g = 1-b;
        xi = data.xhist{i}(t);
        plot(xi(1,:), xi(2,:), '-','Color',[0,g,b],'Linewidth',4);
        plot(xi(1,1), xi(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
        plot(xi(1,end),xi(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
    end
    %plot(xi(1,:), xi(2,:),'go');
    %% Plot optimal trajectory and trajectory iterates
    plot(xsamp(1,:), xsamp(2,:), 'r-','Linewidth',4);
    plot(xsamp(1,1), xsamp(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
    plot(xsamp(1,end),xsamp(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
    set(gca,'LineWidth',2,'FontSize',24);
    axis equal;
    %xlabel('x1');
    %ylabel('x2');
    %title('Trajectory, x1 x2 plane');
    filename = [name,'-traj.eps'];
    saveas(h, filename, 'epsc2');
    close(h);
    
    %% Plot control inputs
    h2 = figure();
    hold on
    plot(t,usamp, 'r-','Linewidth',2);
    for i=1:NN,
        ui = data.uhist{i}(t);
        plot(t,ui,'b-');
    end
    plot(t,ui,'go');
    xlabel('time');
    ylabel('u(t)');
    title('control input');
    filename = [name,'-input.eps'];
    saveas(h2, filename, 'epsc2');
    close(h2);
        
    %% Plot iterates of cost parameters.
    NN = size([data.chist{:}],2);
    h3 = figure();
    %semilogy(sqrt(sum(([data.chist{:}]-repmat(data.c,1,NN)).^2))./norm(data.c));
    semilogy(squared_parm_error);
    xlabel('iteration');
    ylabel('parm error');
    title('parameter error over iterations');
    filename = [name,'-parmerror.eps'];
    saveas(h3, filename, 'epsc2');
    close(h3);
    
    %% Plot iterates of total cost error
    h4 = figure();
    %costerr = [];
    %for i = 1:length(data.costhist),
        %costerr(i) = norm(data.costhist{i} - data.costopt)/abs(data.costopt);
    %end
    %semilogy(costerr);
    semilogy(squared_cost_error);
    xlabel('iteration');
    ylabel('cost error');
    title('cost error over iterations');
    filename = [name,'-costerror.eps'];
    saveas(h4, filename, 'epsc2');
    close(h4);
    
    %% Plot iterates of margin
    h4 = figure();
    semilogy(abs([data.whist{2:end}]));
    xlabel('iteration');
    ylabel('margin, w');
    title('margin over iterations');
    filename = [name, '-margin.eps'];
    saveas(h4, filename, 'epsc2');
    close(h4);
    
    %% Plot distance between feature expectations and demonstration feature expectations.
    h5 = figure();
    %dfeat = [];
    %for i = 1:length(data.feathist),
        %dfeat(i) = norm(data.Vopt - data.feathist{i})/norm(data.Vopt);
    %end
    %semilogy(dfeat,'bo');
    semilogy(featexperrors);
    xlabel('iterations');
    ylabel('feature error');
    title('feature error over iterations');
    filename = [name,'-featerror.eps'];
    saveas(h5, filename, 'epsc2');
    close(h5);
    
    %% Plot improvement beteen iterations.
    h6 = figure();
    deltaw = abs([data.whist{2:end}] - [data.whist{1:end-1}]);
    semilogy(deltaw,'bo');
    xlabel('iterations i');
    ylabel('margin improvement');
    title('margin improvement over iterations');
    filename = [name,'-marginimprov.eps'];
    saveas(h6, filename, 'epsc2');
    close(h6)
end


function plotDirectRun(filename, data, squared_parm_error, squared_cost_error, featexperrors, trajerrors)
    [pathstr, name, ext] = fileparts(filename);
    
    t = linspace(data.t0, data.tf, 100);  % linspace(0,1,100);
    xsamp = data.xopt(t);
    usamp = data.uopt(t);

    h = figure();
    hold on
    NN = length(data.xhist);
    for i=1:NN,
        b = i/NN;
        g = 1-b;
        xi = data.xhist{i}(t);
        plot(xi(1,:), xi(2,:), '-','Color',[0,g,b],'Linewidth',4);
        plot(xi(1,1), xi(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
        plot(xi(1,end),xi(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
    end
    %plot(xi(1,:), xi(2,:),'go');
    %% Plot optimal trajectory and trajectory iterates
    plot(xsamp(1,:), xsamp(2,:), 'r-','Linewidth',4);
    plot(xsamp(1,1), xsamp(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
    plot(xsamp(1,end),xsamp(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
    set(gca,'LineWidth',2,'FontSize',24);
    axis equal;
    %xlabel('x1');
    %ylabel('x2');
    %title('Trajectory, x1 x2 plane');
    filename = [name,'-traj.eps'];
    saveas(h, filename, 'epsc2');
    close(h);
    
    %% Plot control inputs
    h2 = figure();
    hold on
    plot(t,usamp, 'r-','Linewidth',2);
    for i=1:NN,
        ui = data.uhist{i}(t);
        plot(t,ui,'b-');
    end
    plot(t,ui,'go');
    xlabel('time');
    ylabel('u(t)');
    title('control input');
    filename = [name,'-input.eps'];
    saveas(h2, filename, 'epsc2');
    close(h2);
    
    %% Plot iterates of cost parameters.
    NN = size([data.chist{:}],2);
    h3 = figure();
    %plot(sqrt(sum(([data.chist{:}]-repmat(data.c,1,NN)).^2))./norm(data.c));
    semilogy(squared_parm_error);
    xlabel('iteration');
    ylabel('norm(ci - c)');
    title('parameter error over iterations');
    filename = [name,'-parmerror.eps'];
    saveas(h3, filename, 'epsc2');
    close(h3);
    
        
    %% Plot iterates of total cost error
    h4 = figure();
    %costerr = [];
    %for i = 1:length(data.costhist),
        %costerr(i) = norm(data.costhist{i} - data.costopt)/abs(data.costopt);
    %end
    %semilogy(costerr);
    semilogy(squared_cost_error);
    xlabel('iteration');
    ylabel('cost error');
    title('cost error over iterations');
    filename = [name,'-costerror.eps'];
    saveas(h4, filename, 'epsc2');
    close(h4);
    
%     %% Plot iterates of cost
%     h4 = figure();
%     semilogy(abs([data.costhist{1:end}]));
%     xlabel('iteration');
%     ylabel('cost');
%     title('cost over iterations');
%     filename = [name, '-cost.eps'];
%     saveas(h4, filename, 'epsc2');    
%     close(h4);
    
    %% Plot iterates of squared error in trajectory
    h4 = figure();
    %semilogy([data.objectivehist{:}]);
    semilogy(trajerrors);
    xlabel('iteration');
    ylabel('trajectory error');
    title('squared trajectory error over iterations');
    filename = [name,'-trajerror.eps'];
    saveas(h4, filename, 'epsc2');
    close(h4);
end


function plotKKTRun(filename, data)
    [pathstr, name, ext] = fileparts(filename);
    
    t = linspace(data.t0, data.tf, 100); % linspace(0,1,100);


    h = figure();
    hold on
    %% Plot optimal trajectory and trajectory iterates
    NN = length(data.xhist);
    for i=1:NN,
        xsamp = data.xopt{i}(t);
        %xi = data.xhist{i}{1}(t);
        xi = data.xhist{i}(t);
        plot(xi(1,:), xi(2,:), 'b-','Linewidth',4);
        plot(xi(1,1), xi(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
        plot(xi(1,end), xi(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
        plot(xsamp(1,:), xsamp(2,:), 'r-','Linewidth',4);
        plot(xsamp(1,1), xsamp(2,1),'go','MarkerSize',10,'MarkerFaceColor','g');
        plot(xsamp(1,end), xsamp(2,end),'ro','MarkerSize',10,'MarkerFaceColor','r');
    end
    %plot(xi(1,:), xi(2,:),'go');
    %xlabel('x1');
    %ylabel('x2');
    %title('Trajectory, x1 x2 plane');
    set(gca,'LineWidth',2,'FontSize',24);
    axis equal;
    filename = [name,'-traj.eps'];
    saveas(h, filename, 'epsc2');
    close(h);
    
    %% Plot control inputs
    h2 = figure();
    hold on
    for i=1:NN,
        usamp = data.uopt{i}(t);
        plot(t,usamp, 'r-','Linewidth',2);
        %ui = data.uhist{i}{1}(t);
        ui = data.uhist{i}(t);
        plot(t,ui,'b-');
    end
    %plot(t,ui,'go');
    xlabel('time');
    ylabel('u(t)');
    title('control input');
    filename = [name,'-input.eps'];
    saveas(h2, filename, 'epsc2');
    close(h2);
end