function [ out ] = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp)
% out = MaxMargin_IOC()
%

fprintf('\n\nMaxMargin_IOC_lqr3...\n\n');

% number of cost basis functions
k = length(c);  
costopt = c'*Vopt;


%% Solve the inverse optimal control problem for unknown cost function parameters.
% initial guess for unknown parms
chat = 0.1*ones(size(c));
chat(1) = 0.1;            

%% begin iterations.
tic;
chist = {};
xhist = {};
uhist = {};
phist = {};
feathist = {};
costhist = {}
forwardtimes = {};
lambda = 1;
alpha = 1/(5*lambda);
%r = 1;
epsilon = 1e-5; % convergence criterion
run = 1; 
i = 1;
max_iterations = inf;
error_old = inf;
delta_error = inf;
slow_count = 0;
while run,
    % solve forward problem at current value of parameters, chat
    forwardtstart = tic;
    [xi, ui, pi, ~, ~, ~] = lqrMain(t0, tf, x0, xf, chat, A, B);
    forwardtimes{i} = toc(forwardtstart);
    V = FeatureExpectations( xi, ui, t0, tf, @phi_quadratic, k )

    %alpha = 1/(lambda*i);
    chat_new = chat - alpha * (Vopt - V + lambda*chat);  % iterative update step
    chat_new = project(chat_new); % project chat into desired convex set
    %error = norm(chat_new - chat) % check convergence of parameters.
    error = norm(V - Vopt)^2/norm(Vopt)^2;

    delta_error = abs(error - error_old);
    if (error >= error_old)
        alpha = 0.7*alpha;
        if alpha < 1e-6
            break
        end
    end
    
    costhist{i} = chat'*V;
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    feathist{i} = V;
    chist{i} = chat;
    alphahist{i} = alpha;
    
    fprintf('alpha = %e\n',alpha);
    fprintf('costerror = %e\n', norm(costopt - costhist{end}));
    fprintf('featerror = %e\n',norm(Vopt - feathist{end}));
    fprintf('parmerror = %e\n',norm(c - chist{end}));
    
    chat = chat_new;
        
    if (delta_error < epsilon/10)
        if (slow_count > 5)
            break;
        else
            slow_count = slow_count + 1
        end
    else
        slow_count = 0;
    end
    error_old = error;
    run = error > epsilon; % keep running until we satisfy convergence criterion
    
    if length(chist) > 1
        if norm(chist{end} - chist{end-1}) < 1e-4
            run = false;
        end
    end
    if i > max_iterations,
        break;
    end
    i = i+1
end

totalTime = toc;

out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.costopt = costopt;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.chat = chat;
out.whist = {};
out.chist = chist;
out.feathist = feathist;
out.xhist = xhist;
out.uhist = uhist;
out.phist = phist;
out.forwardtimes = forwardtimes;
out.costhist = costhist;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-max-margin3-lqr.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-max-margin3-lqr.mat'];
end
save(filename, 'out');

end

function cproj = project(c)

%cproj = zeros(size(c));
cproj = c;

% project to interval [0.01, 1]^k
% first, normalize first element to 0.1
cproj = 0.1*(cproj/cproj(1));
% then, chop to fit bounds
for i = 2:length(c)
    cproj(i) = max(0.001,c(i));
    cproj(i) = min(10.0, cproj(i));
end

end

