function [ terminalCost, integrandCost, terminalJac, integrandJac ] = unicyclegpopsPerturbCost( sol )

global c k epsilon k2

N = length(sol.time);
t = sol.time;    % N x 1 column vector
x = sol.state;   % N x n matrix, N number of collocation points, n is dimension of state
u = sol.control; % N x m matrix, N number of collocation points, m is dimension of control
p = sol.parameter;

n = length(x(1,:));
m = length(u(1,:));

terminalCost = 0; % terminal cost
integrandCost = zeros(size(t));
terminalJac = [zeros(1,n), 0, zeros(1,n), 0, zeros(1,length(p))];
dL = zeros(N,n+m);
for i = 1:N
    [phi_fourier, dphi_fourier] = phi_fourier_mult( t(i), x(i,:)', u(i,:)', k2 );
    integrandCost(i) = c' * phi_unicycle(t(i),x(i,:)',u(i,:)',k) + epsilon' * phi_fourier;
    dL(i,:) = 2*[c(1)*x(i,1), c(2)*x(i,2), c(3)*x(i,3), c(4)*u(i,1), c(5)*u(i,2)] + epsilon' * dphi_fourier;
end
integrandJac = [ dL, zeros(N,1), zeros(length(t),length(p))];


%n = 3; % state vector dimension
%m = 2; % control input vector dimension

%N = length(sol.time);
%t = sol.time;    % N x 1 column vector
%x = sol.state;  % N x n matrix
%u = sol.control; % N x m matrix, N number of collocation points, m is dimension of control
%xf = sol.terminal.state; % n x 1 vector final state

%Q = diag(c(1:n));
%R = diag(c(n+1:n+m));
%Qf = 1000*eye(3);

%%%%%%% Terminal cost 
%xfbar = [1 0 pi/2]';
%terminalCost = (xf-xfbar)'* Qf*(xf-xfbar); % terminal cost
%terminalCost = 0;

%%%%%%% Integrand cost
%integrandCost = c.*u(:,1).^2 + u(:,2).^2;
%integrandCost = zeros(size(sol.time));
%integrandCost = dot(x', Q*x')' + dot(u', R*u')';
%integrandCost = dot(c1mat,phi(x,xdes_samp),2) + dot(c2mat,psi(u,udes_samp),2);

end

