clear global variables

global c xf;
    
curFolder = pwd;
addpath(curFolder);


%% LQR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');

    t0 = 0;
    tf = 10;
    Nexp = 20;
    
    for i = 1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        n = 3;
        m = 2;
        A = randn(n,n);
        [v,e] = eig(A);
        maxe = max(abs(diag(e)));
        A = A/maxe
        B = randn(n,m)
        x0 = 5*randn(3,1)
        xf = nan;
        c = [0.1; 10.^(-2*rand(n+m-1,1))]
        [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
        Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic)
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt;
        uoptlist{1} = uopt;
        Voptlist{1} = Vopt;
        out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end
%% LQR Perturbation
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');

    t0 = 0;
    tf = 10;
    Nexp = 1;
    
    n = 3;
    m = 2;
    A = randn(n,n);
    [v,e] = eig(A);
    maxe = max(abs(diag(e)));
    A = A/maxe
    B = randn(n,m)
    x0 = 5*randn(3,1)
    xf = nan;
    c = [0.1; 10.^(-2*rand(n+m-1,1))]
    [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
    Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic)
    
    epsilon_mag = 10.^linspace(-5,-2,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');

        k2 = 1; % kk = 1 + 2*k*(n+m)
        epsilon = zeros(1 + 2*k2*(n+m),1);
        epsilon(2) = epsilon_mag(i);
        tguess = linspace(t0,tf,50)';
        xguess = xopt(tguess)';
        uguess = uopt(tguess)';
        %tguess = [t0; tf];
        %xguess = [x0'; [0,0,0]];
        %uguess = [[0 0]; [0 0]];
        [xopt2, uopt2, popt2, ~, ~, ~] = lqrgpopsMain(t0, tf, x0, xf, A, B, epsilon, c, tguess, xguess, uguess);
        Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_quadratic)
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt2;
        uoptlist{1} = uopt2;
        Voptlist{1} = Vopt2;
        out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end
%% LQR Perturbation, multiple observations
if 0,
    clear all
    seed = 1;
    rngstate = rng(seed,'twister');
    
    t0 = 0;
    tf = 10;
    n = 3;
    m = 2;
    Nexp = 10;
    
    c = [0.1; 10.^(-2*rand(n+m-1,1))]
    epsilon_mag = 10^-2;
    
    A = randn(n,n);
    [v,e] = eig(A);
    maxe = max(abs(diag(e)));
    A = A/maxe;
    B = randn(n,m);
    
    for i = 1:Nexp,
        fprintf('i = %d\n',i);
        timestamp = datestr(now,'yyyymmdd-HHMMSS');

        %x0 = 5*randn(3,1);
        x0 = -2.5 + 5*rand(n,1);
        xf = nan;

        k2 = 1; % kk = 1 + 2*k*(n+m)
        epsilon = zeros(1 + 2*k2*(n+m),1);
        epsilon(2) = epsilon_mag;
        [xopt, uopt, ~, ~, ~, ~] = lqrMain(t0, tf, x0, xf, c, A, B);
        tguess = linspace(t0,tf,20)';
        xguess = xopt(tguess)';
        uguess = uopt(tguess)';
        [xopt2, uopt2, popt2, ~, ~, ~] = lqrgpopsMain(t0, tf, x0, xf, A, B, epsilon, c, tguess, xguess, uguess);
        Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_quadratic)
        
        %[xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
        %Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic)
        x0list{i} = x0;
        xflist{i} = xf;
        xoptlist{i} = xopt2;
        uoptlist{i} = uopt2;
        Voptlist{i} = Vopt2;
        %out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        %out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        %out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end

%% LQR PERTURBATION INACCURATE MODEL
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');

    t0 = 0;
    tf = 10;
    Nexp = 1;
    
    n = 3;
    m = 2;
    A = randn(n,n);
    [v,e] = eig(A);
    maxe = max(abs(diag(e)));
    A = A/maxe
    B = randn(n,m)
    
    
    x0 = 5*randn(3,1)
    xf = nan;
    c = [0.1; 10.^(-2*rand(n+m-1,1))]

    
    epsilon_mag = 10.^linspace(-5,-2,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');

        %k2 = 1; % kk = 1 + 2*k*(n+m)
        %epsilon = zeros(1 + 2*k2*(n+m),1);
        %epsilon(2) = epsilon_mag(i);
        
        Ap = randn(n,n);
        [v_p,e_p] = eig(Ap);
        maxe_p = max(abs(diag(e_p)));
        Ap = Ap/maxe_p;
        Ap = Ap * epsilon_mag(i);
        [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A+Ap, B);
        Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic);
    
%         tguess = linspace(t0,tf,50)';
%         xguess = xopt(tguess)';
%         uguess = uopt(tguess)';
%         %tguess = [t0; tf];
%         %xguess = [x0'; [0,0,0]];
%         %uguess = [[0 0]; [0 0]];
%         [xopt2, uopt2, popt2, ~, ~, ~] = lqrgpopsMain(t0, tf, x0, xf, A, B, epsilon, c, tguess, xguess, uguess);
%         Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_quadratic)
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt;
        uoptlist{1} = uopt;
        Voptlist{1} = Vopt;
        out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt, uopt, Vopt, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end


%% LQR PERTURBATION SAMPLED DATA
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');

    t0 = 0;
    tf = 10;
    Nexp = 1;
    
    n = 3;
    m = 2;
    A = randn(n,n);
    [v,e] = eig(A);
    maxe = max(abs(diag(e)));
    A = A/maxe
    B = randn(n,m)
    
    
    x0 = 5*randn(3,1)
    xf = nan;
    c = [0.1; 10.^(-2*rand(n+m-1,1))]

    [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
    Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic);
    
    epsilon_mag = 10.^linspace(-3,0,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        
        % SAMPLE THE OBSERVATION AT VARYING SAMPLE RATES
        sample_t = t0:epsilon_mag(i):tf;
        xopt_s = xopt(sample_t);
        uopt_s = uopt(sample_t);
        
        % SPLINE FIT THE OBSERVATION
        xtmp = csaps(sample_t, xopt_s);
        xopt2 = @(t) fnval(xtmp, t);
        utmp = csaps(sample_t, uopt_s);
        uopt2 = @(t) fnval(utmp, t);
        Vopt2 = FeatureExpectations(xopt2,uopt2,t0,tf,@phi_quadratic);
        
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt2;
        uoptlist{1} = uopt2;
        Voptlist{1} = Vopt2;
        out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end

%% LQR PERTURBATION NOISY SAMPLED DATA
if 1,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');

    t0 = 0;
    tf = 10;
    Nexp = 1;
    
    n = 3;
    m = 2;
    A = randn(n,n);
    [v,e] = eig(A);
    maxe = max(abs(diag(e)));
    A = A/maxe
    B = randn(n,m)
    
    
    x0 = 5*randn(3,1)
    xf = nan;
    c = [0.1; 10.^(-2*rand(n+m-1,1))]

    [xopt, uopt, popt, xoptp, uoptp, poptp] = lqrMain(t0, tf, x0, xf, c, A, B);
    Vopt = FeatureExpectations(xopt,uopt,t0,tf,@phi_quadratic);
    
    epsilon_mag = 10.^linspace(-2,-0.3,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        
        % SAMPLE THE OBSERVATION AT VARYING SAMPLE RATES
        sample_period = 0.1;
        sample_t = 0:sample_period:10; % FIXME  do i add 10+epsilon_mag ?
        xopt_s = xopt(sample_t);
        uopt_s = uopt(sample_t);
        
        % ADD NOISE
        Ns = length(sample_t);
        x_noise = epsilon_mag(i) * randn(n,Ns);
        xopt_s = xopt_s + x_noise;
        
        % SPLINE FIT THE OBSERVATION
        xtmp = csaps(sample_t, xopt_s);
        xopt2 = @(t) fnval(xtmp, t);
        utmp = csaps(sample_t, uopt_s);
        uopt2 = @(t) fnval(utmp, t);
        Vopt2 = FeatureExpectations(xopt2,uopt2,t0,tf,@phi_quadratic);
        
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt2;
        uoptlist{1} = uopt2;
        Voptlist{1} = Vopt2;
        out = Direct_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out1 = MaxMargin_IOC_lqr(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out3 = MaxMargin_IOC_lqr3(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, A, B, timestamp);
        out4 = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, A, B, timestamp);
    end
end


%% Elastica %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    clear global variables
    t0 = 0;
    tf = 1;
    x0 = [0,0,0]';
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 0, 0, 0, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 2, 0, 2, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [-0.0555, -0.1816, 1.3209]';
        c = [1, 2, 0, 2, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.9558, -0.2295, 0.1126]';
        c = [1, 3, 1.2, 0.5, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 1,
        clear global variables
        seed = 1;
        rngstate = rng(seed,'twister');  % initialize random number generator
        M = 1;
        Nexp = 20;
        i = 1;
        while i <= Nexp,
            timestamp = datestr(now,'yyyymmdd-HHMMSS');
            p0 = [-10 + 20*rand(2,1); -2 + 4*rand(1,1)]
            c = [0.1; 10.^(-2*rand(4,1))]
            k = length(c);
            [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
            xf = xopt(1);
            xf(3) = angle_diff(xf(3),0);
            Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k)
            x0list{1} = x0;
            xflist{1} = xf;
            xoptlist{1} = xopt;
            uoptlist{1} = uopt;
            Voptlist{1} = Vopt;
            for j=1:M
                costopt{j} = c'*Voptlist{j};
            end
            tguess = linspace(t0,tf,50)';
            xguess = xopt(tguess)';
            uguess = uopt(tguess)';
            [xopt2, uopt2, popt2, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
            Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_elastica, k);
            fprintf('error between gpops and true: %e\n',norm(Vopt - Vopt2));
            if norm(Vopt-Vopt2) > 1
                continue
            end
            out = Direct_IOC_elastica(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1},tguess, xguess, uguess, timestamp);
            out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1}, tguess, xguess, uguess, timestamp);
            out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1}, tguess, xguess, uguess, timestamp);
            out4 = KKT_IOC_elastica( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, costopt, tguess, xguess, uguess, timestamp);
            i = i+1;
        end
    end
    %% Perturbation
    if 0,
        clear global variables
        seed = 1;
        rngstate = rng(seed,'twister');  % initialize random number generator
        
        M = 1;
        
        p0 = [-10 + 20*rand(2,1); -2 + 4*rand(1,1)]
        c0 = [0.1; 10.^(-2*rand(4,1))]
        
        
        epsilon_mag = 10.^linspace(-5,0,20);
        
        i = 1;
        while i <= length(epsilon_mag),
            timestamp = datestr(now,'yyyymmdd-HHMMSS');

            %k2 = 3; % kk = 1 + 2*k*(n+m)
            %epsilon = zeros(1 + 2*k2*(n+m),1);
            %epsilon(1 + (k2-1)*n + 1) = epsilon_mag(i);
            %[xopt, uopt, popt, ~, ~, ~] = elasticagpopsPerturbMain(t0, tf, x0, xf, c, epsilon, tguess, xguess, uguess);
            c = [c0; epsilon_mag(i)*ones(2,1)];
            k = length(c);
            [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
            Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k)
            xf = xopt(1);
            xf(3) = angle_diff(xf(3),0);
            tguess = linspace(t0,tf,50)';
            xguess = xopt(tguess)';
            uguess = uopt(tguess)';
            [xopt2, uopt2, popt2, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
            Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_elastica, k)
            x0list{1} = x0;
            xflist{1} = xf;
            xoptlist{1} = xopt;
            uoptlist{1} = uopt;
            Voptlist{1} = Vopt(1:5);
            for j=1:M
                costopt{j} = c(1:5)'*Voptlist{j};
            end
            fprintf('error between gpops and true: %e\n',norm(Vopt - Vopt2));
            if norm(Vopt-Vopt2) > 1
                continue
            end
            out = Direct_IOC_elastica(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
            out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
            out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
            out4 = KKT_IOC_elastica( t0, tf, x0list, xflist, c(1:5), xoptlist, uoptlist, Voptlist, costopt, tguess, xguess, uguess, timestamp);
            i = i+1
        end
    end
    
end


%% Unicycle %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');  % initialize random number generator
    n = 3;
    m = 2;
    k = n+m;
    Nexp = 20;
    for i=1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        t0 = 0;
        tf = 8;
        x0 = [0, -2, -pi]' + [2, 4, 2*pi]'.*rand(3,1);
        xf = nan;
        c = [0.1; 10.^(-2*rand(k-1,1))]
        tguess = [t0; tf];
        xguess = [x0'; [0,0,0]];
        uguess = [[0 0]; [0 0]];
        [xopt, uopt, ~,~,~,~] = unicyclegpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
        Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_unicycle, k);
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt;
        uoptlist{1} = uopt;
        Voptlist{1} = Vopt;
        tguess = linspace(t0,tf,50)';
        xguess = xopt(tguess)';
        uguess = uopt(tguess)';
        out = Direct_IOC_unicycle(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
        out1 = MaxMargin_IOC_unicycle(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
        out3 = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
        out4 = KKT_IOC_unicycle( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, tguess, xguess, uguess, timestamp);
    end
end
%% Perturbation Unicycle
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');  % initialize random number generator
    n = 3;
    m = 2;
    k = n+m;
    
    t0 = 0;
    tf = 8;
    x0 = [0, -2, -pi]' + [2, 4, 2*pi]'.*rand(3,1);
    xf = nan;
    c = [0.1; 10.^(-2*rand(k-1,1))]
    tguess = [t0; tf];
    xguess = [x0'; [0,0,0]];
    uguess = [[0 0]; [0 0]];
    [xopt, uopt, ~,~,~,~] = unicyclegpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
    Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_unicycle, k);
    tguess = linspace(t0,tf,50)';
    xguess = xopt(tguess)';
    uguess = uopt(tguess)';
        
    epsilon_mag = 10.^linspace(-5,-1,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        
        k2 = 1; % kk = 1 + 2*k*(n+m)
        epsilon = zeros(1 + 2*k2*(n+m),1);
        epsilon(2) = epsilon_mag(i);
        
        [xopt2, uopt2, popt2, ~, ~, ~] = unicyclegpopsPerturbMain(t0, tf, x0, xf, c, epsilon, tguess, xguess, uguess);
        Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_unicycle, k)
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt2;
        uoptlist{1} = uopt2;
        Voptlist{1} = Vopt2;
        tguess = linspace(t0,tf,50)';
        xguess = xopt2(tguess)';
        uguess = uopt2(tguess)';
        out = Direct_IOC_unicycle(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out1 = MaxMargin_IOC_unicycle(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out3 = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out4 = KKT_IOC_unicycle( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, tguess, xguess, uguess, timestamp);
    end
end





%% Locomotion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    % state x: (x,y,theta, vforw, vside, omega)
    % input u: (u1, u2, u3)   forward, sideward, and rotational accels
    t0 = 0;
    tf = 3;
    x0 = [0,0,pi/2,0,0,0]';
    timestamp = datestr(now,'yyyymmdd-HHMMSS');
    %xf = [-2, 1, 3*pi/4, 0, 0, 0]';  % Mombaur's target 2
    xf = [3,1,pi/4,0,0,0]';  % Mombaur's target 1
    %xf = [3, -1, -pi/4, 0, 0, 0]';
    %c = [1, 1.2, 1.7, 0.7, 5.2]'; % Mombaur's final "good" values
    %c = [1.0, 1.4, 0.58, 4.3]';
    c = [1.0, 1.4, 0.58]';
    tguess = [t0; tf];
    xguess = [x0'; xf'];
    uguess = [[0,0,0]; [0,0,0]];
    tic
    [xopt, uopt, ~, ~, ~, ~,tfopt] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
    toc
    Vopt = FeatureExpectations(xopt,uopt,t0,tfopt,@phi_locomotion, xf);
    %         tguess = linspace(t0,tf,50)';
    %         xguess = xopt(tguess)';
    %         uguess = uopt(tguess)';
    %         tf = nan;
    %         [xopt2, uopt2, ~, ~, ~, ~,tfopt2] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
    %tguess = linspace(0,tfopt,50)';
    %%xguess = zeros(length(tguess), 7);
    %%xguess(:,2) = linspace(0,xf(1),50)';
    %%ugues = zeros(length(tguess), 3);
    %xguess = xopt(tguess)';
    %uguess = uopt(tguess)';
    %Mx = [tguess, xguess, zeros(length(tguess),1)];
    %Mu = [tguess, uguess];
    %tic
    %[x1, u1, ~, ~, ~, ~, tfopt2] = locomotionacadoMain(t0, tf, x0, xf, c, Mx, Mu);
    %toc
    x0list{1} = x0;
    xflist{1} = xf;
    Voptlist{1} = Vopt;
    xoptlist{1} = xopt;
    uoptlist{1} = uopt;
    %out = KKT_IOC_locomotion(t0, tfopt, x0list, xflist, c, xoptlist, uoptlist, Voptlist, timestamp);
    %out = Direct_IOC_locomotion(t0, tf, x0, xf, c, xopt, uopt, tfopt, Vopt, tguess, xguess, uguess, timestamp);
    %out1 = MaxMargin_IOC_locomotion(t0, tf, x0, xf, c, timestamp);
    %out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
    %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
    %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
end


