function [ out ] = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp)
% out = MaxMargin_IOC()
%


%t0 = 0;
%tf = 1;
%x0 = [0, 0, 0]';
%xf = [0.4922, -0.5769, -1.8326]';
%xf = [0.1687, -0.6698, 0.4209]';
%xf = [-0.0555; -0.1816;1.3209];

% True cost function parameters, and associated optimal trajectory
%c = [1,0,0.5]';
k = length(c);  % number of cost basis functions

% Initial guess for shooting method: straight line
[xopt, uopt, ~, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c);
Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
costopt = c' * Vopt;

% tsamp = linspace(0,1,50)';
% xsamp = zeros(length(tsamp), 3);
% xsamp(:,2) = linspace(0,1,50)';
% usamp = zeros(length(tsamp), 1);
% Mx = [tsamp, xsamp];
% Mu = [tsamp, usamp];
% [xopt, uopt, ~, ~, ~, ~] = elasticaacadoMain(t0, tf, x0, xf, c, Mx, Mu);
% Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
% costopt = c' * Vopt;

%% Compute the cost of the trajectory:
%cost1 = @(t) c'*psi(t,uopt(t));
%out = quad(myfun, t0, tf);

%plotUnicycle(t0, tf, xopt);

%% Solve the inverse optimal control problem for unknown cost function parameters.
c0 = zeros(size(c));
c0(1) = 1;
tic;
[chist, whist, xhist, uhist, phist, feathist, costhist, forwardtimes] = maxmargin_elastica_a( t0, tf, x0, xf, c0, xopt, uopt);
totalTime = toc;

out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.costopt = costopt;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.chist = chist;
out.whist = whist;
out.xhist = xhist;
out.uhist = uhist;
out.phist = phist;
out.feathist = feathist;
out.costhist = costhist;
out.forwardtimes = forwardtimes;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-max-margin-elastica_a.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-max-margin-elastica_a.mat'];
end

save(filename, 'out');

end

