function [ out ] = MaxMargin_IOC_locomotion(t0, tf, x0, xf, c, xopt, uopt, Vopt, timestamp)

k = length(c);  % number of cost basis functions
costopt = c' * Vopt;


%% Solve the inverse optimal control problem for unknown cost function parameters.
c0 = zeros(size(c));
c0(1) = 1;
tic;


i = 1; % counter to keep track of number of iterations


% Solve forward problem for initial cost function guess.
forwardstart = tic;
[x1, u1, p1, ~, ~, ~] = locomotiongpopsMain(t0, tf, x0, xf, c0);
forwardtimes{i} = toc(forwardstart);
feathist{i} = FeatureExpectations(x1, u1, t0, tf, @phi_locomotion, xf);
costhist{i} = c0' * feathist{i};


% Initialize some arrays that we will be using.
xhist{i} = x1; % optimal trajectory iterates
uhist{i} = u1; % optimal control iterates
phist{i} = p1; % optimal costate iterates
chist{i} = c0;
whist{i} = 1;

epsilon = 1e-3;  % threshold for terminating iterations.

% Linear inequality constrains: A c <= b
A = [];
b = [];

% Constrain the parameters to be positive: A2 c <= b
% Note that we constrain the control cost to be bounded away from zero 
% to avoid numerical issues with forward problem solver.
% This would all be handled more gracefully if we use unconstrained
% parameters, and then project them onto a desired parameter subspace
% (Ratliff et al.).
A2 = -eye(k); 
A2(k+1,k+1) = 0;
b2 = zeros(k+1,1);  
b2(2) = -0.01;
b2(3) = -0.01;
b2(4) = -0.01;


% Constrain the first cost function parameter to be equal to "1".  
% This is our standard normalization step.
Aeq = zeros(1,k+1);
Aeq(1) = 1.0;
beq = 1;

i = 2; % we've already initialized stuff
imax = inf;

% initial guess of unknown parms.  note that we append a guess of zero
% for the unknown "margin" for the fmincon optimization problem.
c0hat = [c0; 0]; 
options = optimset('Algorithm','interior-point','TolFun',1e-4,'TolCon',1e-4,'TolX',1e-4);
while(abs(whist{end}) > epsilon),
    % list of linear inequality constraints: A x <= b  where "x" is the
    % optimization variable, in our case "x" = c.
    A = [A, [Vopt - feathist{end}; -1]];  % append new linear inequality constraint
    b = zeros(i-1,1); 
    % Solve quadratic program (SVM problem).
    cstari = fmincon( @(c)c(end), c0hat, [A';A2], [b; b2], Aeq, beq, [], [], @maxmargin_nonlincon, options)
    whist{i} = cstari(end);
    chist{i} = cstari(1:end-1);
    % cut iterations off if convergence is taking too long
    if i > imax
        break;
    end
    % Solve forward problem given:  start and goal state, cost func parms c.
    forwardstart = tic;
    [xi, ui, pi, ~, ~, ~] = locomotiongpopsMain(t0, tf, x0, xf, chist{i});
    forwardtimes{i} = toc(forwardstart);
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    feathist{i} = FeatureExpectations(xi,ui,t0,tf,@phi_locomotion,xf);
    costhist{i} = chist{i}' * feathist{i};
    i = i+1
end % end while loop

%[chist, whist, xhist, uhist, phist, feathist, costhist, forwardtimes] = maxmargin_locomotion( t0, tf, x0, xf, c0, xopt, uopt);


totalTime = toc;

out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.costopt = costopt;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.chist = chist;
out.whist = whist;
out.xhist = xhist;
out.uhist = uhist;
out.phist = phist;
out.feathist = feathist;
out.costhist = costhist;
out.forwardtimes = forwardtimes;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-max-margin-locomotion.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-max-margin-locomotion.mat'];
end

save(filename, 'out');

end

