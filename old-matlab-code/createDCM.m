[dcm] = createDCM(v1, v2)
%
% createDCM(v1, v2)
%
%   v1, v2 are matrices containing 3 orthogonal unit vectors
%   this function computes the rotation matrix between them.
%

dcm = [v1(
