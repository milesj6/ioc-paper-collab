function [x, u, p, conf] = getElastica3DGivenP0(x0, p0, c)


%%%%%%%%%% set up global variables %%%%%%%%%
%global p0 k2 c2
%
%p0 = [20 -20 6]'; % initial costate
%k2 = 1 + 2*4;  % number of basis functions in cost
%c2 = [1 0 0 0 0 0 0 0 0]';
%%%%%%%%%% end global variables %%%%%%%%%

n = 3;
m = 1;
k = length(c);
t0 = 0;
tf = 1.0;


% Basis for the Lie algebra
X{1} = [0 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 0];
X{2} = [0 0 1 0; 0 0 0 0; -1 0 0 0; 0 0 0 0];
X{3} = [0 -1 0 0; 1 0 0 0; 0 0 0 0; 0 0 0 0];
X{4} = [0 0 0 1; 0 0 0 0; 0 0 0 0; 0 0 0 0];
X{5} = [0 0 0 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
X{6} = [0 0 0 0; 0 0 0 0; 0 0 0 1; 0 0 0 0];

% Tolerance when checking for the event that det(J) has a zero crossing
tolStability = 1e-8;

% Parameters that govern collision checking...
tolCollision = 2^-4;
wCollision = 0.75*(2^(-5));
sCollision = 5*wCollision;


%options = odeset('RelTol',1e-6,'AbsTol',1e-8);
%sol = ode45(@(t,x)elasticaEOM(t, x, @(t,x)elasticaMuCostate0(t,x,p0,c)), [t0 tf], x0, options);
conf = struct;
musol = GetMU(p0,c);
MJsol = GetMJ(musol,tolStability);
nt = 100;
t = linspace(0,1,nt);
MJ = deval(MJsol,t);
J = MJ(37:72,:);
detJ = zeros(1,nt);
for i=1:nt
    detJ(i) = det(reshape(J(:,i),6,6));
end
conf.a = p0;
conf.musol = musol;
conf.MJsol = MJsol;
conf.nt = nt;
conf.t = t;
conf.J = J;
conf.detJ = detJ;
conf.qsol = GetQ(conf.musol,X,c);
conf.q = deval(conf.qsol,t);
conf.qpos = conf.q(13:15,:);
conf.u = deval(conf.musol,t)


% Check for instability
[res,tconj] = IsUnstable(MJsol);
conf.stable = ~res;

% Check for collision
[res,B1tmp,B2tmp] = InCollision(conf,tolCollision,wCollision,sCollision);
conf.collisionfree = ~res;

% Check for infeasibility
conf.infeasible = ~(conf.stable&&conf.collisionfree)

D = UpdateDisplay(conf);

tsamp = linspace(0,1,10);
deval(conf.musol,tsamp)

x = @(t) deval(conf.qsol,t);
u = @(t) uFromMu(t,conf.musol, c);
%u = @(t) elasticaMuCostate0(t,x(t),p0,c);
%p = @(t) [p0(1); p0(2); x(t)'.*[-p0(2), p0(1), 0]' + p0(3)];
%p = @(t) vertcat(repmat([p0(1);p0(2)],1,length(t)), (x(t)'*[-p0(2), p0(1), 0]')' + p0(3));
p = nan;

%t = linspace(0,1,100)';
%N = length(t);
%xhist = zeros(N,n);
%uhist = zeros(N,m);
%for i = 1:N
%    xhist(i,:) = deval(sol,t(i));
%    uhist(i) = elasticaMuCostate0(t(i),xhist(i,:)',p0,c);
%end

    
end

function u = uFromMu(t,musol,c)
mu = deval(musol,t);
u = mu(1:3,:) ./ repmat(c,1,length(t));
end

function sol = GetMU(a,c)
options = odeset('RelTol',1e-6,'AbsTol',1e-8);
sol = ode45(@(t,x) fMU(t,x,c), [0,1], a, options);
end

function mudot = fMU(t,mu,c)
% mudot = [0;
%     mu(6);
%     -mu(5);
%     mu(3)*mu(5)-mu(2)*mu(6);
%     -mu(3)*mu(4)+mu(1)*mu(6);
%     mu(2)*mu(4)-mu(1)*mu(5)];

u1 = mu(1) / c(1);
u2 = mu(2) / c(2);
u3 = mu(3) / c(3);

mudot = [u3 * mu(2) - u2*mu(3);
         mu(6) + u1*mu(3) - u3*mu(1);
         -mu(5) + u2*mu(1) - u1*mu(2);
         u3*mu(5) - u2*mu(6);
         u1*mu(6) - u3*mu(4);
         u2*mu(4) - u1*mu(5);];
end

function sol = GetQ(musol, X, c)
sol = ode45(@(t,x) fQ(t,x,musol,X,c),[0,1],reshape(eye(4),16,1),odeset('reltol',1e-4,     'abstol',1e-8));
end

function qdot = fQ(t,q,musol,X,c)
q = reshape(q,4,4);
mu = deval(musol,t);
u = mu(1:3) ./ c;
qdot = q*(u(1)*X{1}+u(2)*X{2}+u(3)*X{3}+X{4});
qdot = reshape(qdot,16,1);
end

function sol = GetMJ(musol,tol)
sol = ode45(@(t,x) fMJ(t,x,musol),[0,1],reshape([eye(6) zeros(6)],72,1),odeset('reltol',1e-4,'abstol',1e-8,'events',@(t,x) conjMJ(t,x,tol)));
end

function MJdot = fMJ(t,MJ,musol)
mu = deval(musol,t);
F = [0 0 0 0 0 0;
     0 0 0 0 0 1;
     0 0 0 0 -1 0;
     0 -mu(6) mu(5) 0 mu(3) -mu(2);
     mu(6) 0 -mu(4) -mu(3) 0 mu(1);
     -mu(5) mu(4) 0 mu(2) -mu(1) 0];
G = diag([1 1 1 0 0 0]);
H = [0 mu(3) -mu(2) 0 0 0;
     -mu(3) 0 mu(1) 0 0 0;
     mu(2) -mu(1) 0 0 0 0;
     0 0 0 0 mu(3) -mu(2);
     0 0 1 -mu(3) 0 mu(1);
     0 -1 0 mu(2) -mu(1) 0];
MJ = reshape(MJ,6,12);
M = MJ(:,1:6);
J = MJ(:,7:12);
MJdot = [F*M G*M+H*J];
MJdot = reshape(MJdot,72,1);
end

function [res,tconj] = IsUnstable(solz)
ithreshold = find(solz.ie==2,1,'first');
res = 0;
tconj = [];
if (~isempty(ithreshold))
    iconj = find(solz.ie==1);
    iconj = iconj(iconj>ithreshold);
    if (~isempty(iconj))
        iconj = iconj(1);
        tconj = solz.xe(iconj);
        res = 1;
    end
end
end

function [value,isterminal,direction] = conjMJ(t,x,tol)
detJ = det(reshape(x(37:72,1),6,6));
value = [detJ; abs(detJ)-tol];
isterminal = [0; 0];
direction = [0; 0];
end