function [ dae, deriv ] = locomotiongpopsDAE( sol )

% state x: (x,y,theta, vforw, vside, omega)
% input u: (u1, u2, u3)   forward, sideward, and rotational accels

t = sol.time; % N x 1 col vector
x = sol.state; % N x n matrix
u = sol.control; % N x m matrix
p = sol.parameter;

zerost = zeros(size(t));

%dae = [u(:,1).*cos(x(:,3)) u(:,1).*sin(x(:,3)) u(:,2)];
dae = [x(:,4).*cos(x(:,3)) - x(:,5).*sin(x(:,3)), ...
        x(:,4).*sin(x(:,3)) + x(:,5).*cos(x(:,3)), ...
        x(:,6), ...
        u(:,1), ...
        u(:,2), ...
        u(:,3)];

% Jacobian
df1_dx1 = zerost;
df1_dx2 = zerost;
df1_dx3 = -x(:,4).*sin(x(:,3)) - x(:,5).*sin(x(:,3));
df1_dx4 = cos(x(:,3));
df1_dx5 = -sin(x(:,3));
df1_dx6 = zerost;
df2_dx1 = zerost;
df2_dx2 = zerost;
df2_dx3 = x(:,4).*cos(x(:,3)) - x(:,5).*sin(x(:,3));
df2_dx4 = sin(x(:,3));
df2_dx5 = cos(x(:,3));
df2_dx6 = zerost;
df3_dx1 = zerost;
df3_dx2 = zerost;
df3_dx3 = zerost;
df3_dx4 = zerost;
df3_dx5 = zerost;
df3_dx6 = ones(size(t));
df4_dx1 = zerost;
df4_dx2 = zerost;
df4_dx3 = zerost;
df4_dx4 = zerost;
df4_dx5 = zerost;
df4_dx6 = zerost;
df5_dx1 = zerost;
df5_dx2 = zerost;
df5_dx3 = zerost;
df5_dx4 = zerost;
df5_dx5 = zerost;
df5_dx6 = zerost;
df6_dx1 = zerost;
df6_dx2 = zerost;
df6_dx3 = zerost;
df6_dx4 = zerost;
df6_dx5 = zerost;
df6_dx6 = zerost;
df1_du1 = zerost;
df1_du2 = zerost;
df1_du3 = zerost;
df2_du1 = zerost;
df2_du2 = zerost;
df2_du3 = zerost;
df3_du1 = zerost;
df3_du2 = zerost;
df3_du3 = zerost;
df4_du1 = ones(size(t));
df4_du2 = zerost;
df4_du3 = zerost;
df5_du1 = zerost;
df5_du2 = ones(size(t));
df5_du3 = zerost;
df6_du1 = zerost;
df6_du2 = zerost;
df6_du3 = ones(size(t));

df1_dp = zeros(length(t),length(p));
df2_dp = zeros(length(t),length(p));
df3_dp = zeros(length(t),length(p));
df4_dp = zeros(length(t),length(p));
df5_dp = zeros(length(t),length(p));
df6_dp = zeros(length(t),length(p));
df1_dt = zerost;
df2_dt = zerost;
df3_dt = zerost;
df4_dt = zerost;
df5_dt = zerost;
df6_dt = zerost;

deriv = [df1_dx1, df1_dx2, df1_dx3, df1_dx4, df1_dx5, df1_dx6, df1_du1, df1_du2, df1_du3, df1_dt, df1_dp; ...
         df2_dx1, df2_dx2, df2_dx3, df2_dx4, df2_dx5, df2_dx6, df2_du1, df2_du2, df2_du3, df2_dt, df2_dp; ...
         df3_dx1, df3_dx2, df3_dx3, df3_dx4, df3_dx5, df3_dx6, df3_du1, df3_du2, df3_du3, df3_dt, df3_dp; ...
         df4_dx1, df4_dx2, df4_dx3, df4_dx4, df4_dx5, df4_dx6, df4_du1, df4_du2, df4_du3, df4_dt, df4_dp; ...
         df5_dx1, df5_dx2, df5_dx3, df5_dx4, df5_dx5, df5_dx6, df5_du1, df5_du2, df5_du3, df5_dt, df5_dp; ...
         df6_dx1, df6_dx2, df6_dx3, df6_dx4, df6_dx5, df6_dx6, df6_du1, df6_du2, df6_du3, df6_dt, df6_dp];

end

