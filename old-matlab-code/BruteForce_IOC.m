
function [c_hat, Objective_value] = BruteForce_IOC()

options = optimset;



figure(1), hold
figure(2), hold
global iter ;
iter = 1;


    t0 = 0;
    tf = 1;
    x0 = [0,0,0]';
    xf = [0.1687, -0.6698, 0.4209]';
    c_opt = [1, 2, 0, 2, 0]';

c_0 =  [0, 0, 0, 0]';
c_hat_first = 1;
    
x_observed = Forward_Problem(t0, tf, x0, xf,c_opt);

[c_hat,Objective_value] = fminsearch(@(c_hat_remainder)BruteForce_Objective(c_hat_remainder, c_hat_first, c_opt, t0,tf,x0,xf,x_observed),c_0,options);
