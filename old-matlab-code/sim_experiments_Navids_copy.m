function [all_errors, all_ms_errors, All_c_real, All_c_cont, All_c_samp] = sim_experiments_Navids_copy(number_of_samples, sigma_sq)

clear global variables

global c xf;
    
curFolder = pwd;
addpath(curFolder);




all_errors = [];
all_ms_errors = [];

%% Elastica %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 1,
    clear global variables
    t0 = 0;
    tf = 1;
    x0 = [0,0,0]';
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 0, 0, 0, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 2, 0, 2, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [-0.0555, -0.1816, 1.3209]';
        c = [1, 2, 0, 2, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.9558, -0.2295, 0.1126]';
        c = [1, 3, 1.2, 0.5, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 1,
        clear global variables
        seed = 1;
        %rngstate = rng(seed,'twister');  % initialize random number generator
        M = 1;
        Nexp = 600;
        i = 1;
        while i <= Nexp,
            timestamp = datestr(now,'yyyymmdd-HHMMSS');
            p0 = [-10 + 20*rand(2,1); -2 + 4*rand(1,1)];
            c = [0.1; 10.^(-2*rand(4,1))];
            k = length(c);
            [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
            xf = xopt(1);
            xf(3) = angle_diff(xf(3),0);
            Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k);
            x0list{1} = x0;
            xflist{1} = xf;
            xoptlist{1} = xopt;
            uoptlist{1} = uopt;
            Voptlist{1} = Vopt;
            for j=1:M
                costopt{j} = c'*Voptlist{j};
            end
             tguess = linspace(t0,tf,50)';
            xguess = xopt(tguess)';
            uguess = uopt(tguess)';
%             [xopt2, uopt2, popt2, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
%             Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_elastica, k);
%             fprintf('error between gpops and true: %e\n',norm(Vopt - Vopt2));
%             if norm(Vopt-Vopt2) > 1
%                 continue
%             end
            %out = Direct_IOC_elastica(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1},tguess, xguess, uguess, timestamp);
            %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1}, tguess, xguess, uguess, timestamp);
            %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, xopt, uopt, Vopt, costopt{1}, tguess, xguess, uguess, timestamp);
            
            % Navid
            % Sample Data
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            t_sampled = linspace(t0, tf, number_of_samples);
            xoptlist_sampled = xoptlist{1}(t_sampled);
            uoptlist_sampled = uoptlist{1}(t_sampled);
            
%             figure(9), hold on, plot(xoptlist_sampled');

            
            % add measurement noise
            xoptlist_sampled = xoptlist_sampled + sqrt(sigma_sq)*randn(size(xoptlist_sampled));
%             uoptlist_sampled = uoptlist_sampled + sqrt(sigma)*randn(size(uoptlist_sampled));

%              figure(10), hold on, plot(xoptlist_sampled');
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            
            % SAMPLED DATA - Spline fitting
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %               This right here works for sampled data, but I want to
% %               modify to see if partial data works too!
%             xoptlist_cont = csaps(t_sampled, xoptlist_sampled, 1);
%             uoptlist_cont = csaps(t_sampled, uoptlist_sampled, 1);
%             
%             xoptlist_cont2{1} = @(t) fnval(xoptlist_cont,t);
%             uoptlist_cont2{1} = @(t) fnval(uoptlist_cont,t);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


            % PARTIAL AND SAMPLED DATA - Deriving unobserved using splines
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % since we are now adding noise, I should fit a spline and
            % resample to get x_3
%             xoptlist_cont = csaps(t_sampled, xoptlist_sampled, 1);
            xoptlist_cont = csaps(t_sampled, xoptlist_sampled, 0.999);
%             [xoptlist_cont, p_guessed] = csaps(t_sampled, xoptlist_sampled);
%             p_guessed
%             xoptlist_cont = csaps(t_sampled, xoptlist_sampled, 0.999); % THIS SEEMS TO WORK! very important because it affects the atan func 
            % we don't want the spline to fit very closely the sampled
            % data, because it's noisy and there are non-smooth parts, but
            % we don't want it to do too much smoothness not to look like
%             the original...this 0.999 seems to work nicely

            xoptlist_cont2{1} = @(t) fnval(xoptlist_cont,t);
            
            HIGH_samples = 100000;
            HIGH_t_sampled = linspace(t0, tf, HIGH_samples);
            
            xoptlist_sampled = xoptlist_cont2{1}(HIGH_t_sampled);

%              figure(11), hold on, plot(xoptlist_sampled');

            
            
            xoptlist_sampled_unobs = xoptlist_sampled(3, :);
            xoptlist_sampled = xoptlist_sampled([1,2],:);
            
            % maybe ideally I want to apply atan to a spline and not
            % sampled data -- Yes I do!!!
            % for now maybe I can just resample by a lotttt!!!
            xoptlist_sampled_x3_created = atan(   diff(xoptlist_sampled(2,:))  ./ diff(xoptlist_sampled(1,:))    );
            
            while find( abs(diff(xoptlist_sampled_x3_created)) > pi/1.2)
                
%                 figure(12), subplot(3,1,1),plot((180/pi)*[xoptlist_sampled_x3_created; xoptlist_sampled_unobs(1:end-1)]')
                
                [aa, bb] = find( (diff(xoptlist_sampled_x3_created)) <  - pi/1.2);
                [cc, dd] = find( (diff(xoptlist_sampled_x3_created)) >  + pi/1.2);
                
                if length(bb) > 0 && length(dd) > 0
                    if (bb(1) < dd(1))
                        xoptlist_sampled_x3_created(bb(1) + 1:end) = xoptlist_sampled_x3_created(bb(1) + 1:end) + pi;
                    else
                        xoptlist_sampled_x3_created(dd(1) + 1:end) = xoptlist_sampled_x3_created(dd(1) + 1:end) - pi;
                    end
                elseif length(bb) > 0
                    xoptlist_sampled_x3_created(bb(1) + 1:end) = xoptlist_sampled_x3_created(bb(1) + 1:end) + pi;
                elseif length(dd) > 0
                    xoptlist_sampled_x3_created(dd(1) + 1:end) = xoptlist_sampled_x3_created(dd(1) + 1:end) - pi;
                end
            end
            
            
            xoptlist_sampled_created = [xoptlist_sampled(:, 1:end-1) ; xoptlist_sampled_x3_created];
            t_sampled2 = linspace(t0, tf, HIGH_samples - 1 );
            xoptlist_cont = csaps(t_sampled2, xoptlist_sampled_created, 1);
            xoptlist_cont2{1} = @(t) fnval(xoptlist_cont,t);
            
            
%             x3_cont  = csaps(t_sampled, xoptlist_sampled_unobs, 1);
%             uoptlist_cont2{1} = @(t) fnval(x3_cont,t);
%             uoptlist_cont2{1} = @(t) diff(uptlist_cont2{1});
            
%             uoptlist_cont2{1} = @(t) deval(uoptlist_cont2,t);
%             uoptlist_cont2{1} = @(t) deval(x3_cont, t);
%             plot([xoptlist_sampled_x3; xoptlist_sampled_unobs(1:end-1)]')
            
            % ideally I would want to differentiate the spline x3_cont, and
            % not do a silly diff on the sampled version!
%             t_dense = linspace(t0, tf, 1000);
%             finely_sampled_x3 = xoptlist{1}(t_dense);
%             finely_sampled_x3 = finely_sampled_x3(3,:);
            
            t_sampled3 = linspace(t0, tf, HIGH_samples - 2 );
            u_sampled_created = diff(xoptlist_sampled_unobs)./diff(HIGH_t_sampled);
%             u_sampled_created = diff(xoptlist_sampled_x3_created);
            
%             t_sampled3 = linspace(t0, tf, number_of_samples - 2 );
            % if u is not observed
            uoptlist_cont =  csaps(t_sampled2, u_sampled_created, 1);
            % if u is observed 
%             uoptlist_cont = csaps(t_sampled, uoptlist_sampled, 1);
            
            uoptlist_cont2{1} = @(t) fnval(uoptlist_cont, t);
            
%             % Debugging x_3_created
%             figure(13)
%             subplot(2,1,1),plot((180/pi)*[xoptlist_sampled_x3_created; xoptlist_sampled_unobs(1:end-1)]')
% %             subplot(2,1,2),plot([u_sampled_created; uoptlist_sampled(1:end-1)]')
%             subplot(3,1,2),plot( [diff(xoptlist_sampled(2,:))  ./ diff(xoptlist_sampled(1,:)) ; tan(xoptlist_sampled_unobs(1:end-1)) ]')
%             subplot(3,1,3),plot( [diff(xoptlist_sampled(2,:))  ; diff(xoptlist_sampled(1,:))]')

            % Debugging u_created
%             subplot(2,1,2),plot((180/pi)*[u_sampled_created; uoptlist_sampled(1:end-1)]')
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
%             [percentage_error, ms_error] = DF_IOC_elastica_Navid( t0, tf, x0list, xflist, c, xoptlist_cont2, uoptlist_cont2, Voptlist, costopt, tguess, xguess, uguess, timestamp);
            [percentage_error, ms_error] = DF_IOC_elastica_Navid( t0, tf, x0list, xflist, c, xopt, uopt, Voptlist, costopt, tguess, xguess, uguess, timestamp);


            [percentage_error, ms_error] = KKT_IOC_elastica_Navid( t0, tf, x0list, xflist, c, xoptlist_cont2, uoptlist_cont2, Voptlist, costopt, tguess, xguess, uguess, timestamp);
            %out4 = KKT_IOC_elastica_Navid( t0, tf, x0list, xflist, c, xoptlist_cont2, Voptlist, costopt, tguess, xguess, uguess, timestamp);
            i = i+1;
            
            all_errors = [all_errors ; percentage_error];
            all_ms_errors = [all_ms_errors; ms_error];
        end
    end
    
end

%% 3D Elastica %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    
    
    All_c_real = [];
    All_c_cont = [];
    All_c_samp = [];
    
    clear global variables
    t0 = 0;
    tf = 1;
%     x0 = [0,0,0]';
%     x0 = [0, 0, 0, 0, 0, 0]';
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 0, 0, 0, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.1687, -0.6698, 0.4209]';
        c = [1, 2, 0, 2, 0]';
        %out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [-0.0555, -0.1816, 1.3209]';
        c = [1, 2, 0, 2, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 0,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        xf = [0.9558, -0.2295, 0.1126]';
        c = [1, 3, 1.2, 0.5, 0]';
        out = Direct_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c, timestamp);
        %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
        out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
    end
    if 1,
        clear global variables
        seed = 1;
        rngstate = rng(seed,'twister');  % initialize random number generator
        M = 1;
        Nexp = 1;
        i = 1;
        while i <= Nexp,
%             timestamp = datestr(now,'yyyymmdd-HHMMSS');
            p0_fake = [-10 + 20*rand(4,1); -2 + 4*rand(2,1)]
            c = [0.1; 10.^(-2*rand(2,1))];
            k = length(c);
            
            x0 = eye(4);
%             a  = [0, 0, 1, 0, 0, 0]';
            a  = 0.1*[1, 1, 2, 1, 2, 0]';
%             a = p0;
            

            for j = 1:M
                
%                 a = [-10 + 20*rand(4,1); -2 + 4*rand(2,1)];
                a  = 0.1*[1, 1, 2, 1, 2, 0]' + 0.1*rand(6,1);
                p0{j} = a;
    %             [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
                [ q, u, popt, J, qdot ] = elasticaFromInitialCostate( x0, p0{j}, c );

    %             drawElastica3D({q});


                xf = q(1);
                xf(3) = angle_diff(xf(3),0);
    %             Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k)
                x0list{1} = x0;
                xflist{1} = xf;
    %             xoptlist{1} = xopt;
    %             uoptlist{1} = uopt;
    %             t_plot = linspace(0,1, 1000);
    %             plot(q(t_plot)')



                % DO IOC for continuous and sampled data
                No_of_Samples = number_of_samples;
                [u_est, q_est, qdot_est] = uFromq(q, qdot, u, No_of_Samples);

                q_cont{j} = q;
                u_cont{j} = u;

                q_samp{j} = q_est;
                u_samp{j} = u_est;
                
            end


            chat_continuous_data = KKT_IOC_elastica3D( 0, 1, c, q_cont, u_cont, a);
            chat_continuous_data

            chat_sampled_data = KKT_IOC_elastica3D( 0, 1, c, q_samp, u_samp, a);
            chat_sampled_data
%             chat_sampled_data = 0;

%             Voptlist{1} = Vopt;
%             for j=1:M
%                 costopt{j} = c'*Voptlist{j};
%             end
%             tguess = linspace(t0,tf,50)';
%             xguess = xopt(tguess)';
%             uguess = uopt(tguess)';
%             [xopt2, uopt2, popt2, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
%             Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_elastica, k);
%             fprintf('error between gpops and true: %e\n',norm(Vopt - Vopt2));
%             if norm(Vopt-Vopt2) > 1
%                 continue
%             end
%             c_hat_cont = KKT_IOC_elastica3D( t0, tf, c, xoptlist, uoptlist, a);
            
%             c_hat_samp = 0;
            
            All_c_real = [All_c_real ; c'];
            All_c_cont = [All_c_cont ; chat_continuous_data'];
            All_c_samp = [All_c_samp ; chat_sampled_data'];

            i = i+1;
        end
    end

end


    %% Perturbation
%     if 0,
%         clear global variables
%         seed = 1;
%         rngstate = rng(seed,'twister');  % initialize random number generator
%         
%         M = 1;
%         
%         p0 = [-10 + 20*rand(2,1); -2 + 4*rand(1,1)]
%         c0 = [0.1; 10.^(-2*rand(4,1))]
%         
%         
%         epsilon_mag = 10.^linspace(-5,0,20);
%         
%         i = 1;
%         while i <= length(epsilon_mag),
%             timestamp = datestr(now,'yyyymmdd-HHMMSS');
% 
%             %k2 = 3; % kk = 1 + 2*k*(n+m)
%             %epsilon = zeros(1 + 2*k2*(n+m),1);
%             %epsilon(1 + (k2-1)*n + 1) = epsilon_mag(i);
%             %[xopt, uopt, popt, ~, ~, ~] = elasticagpopsPerturbMain(t0, tf, x0, xf, c, epsilon, tguess, xguess, uguess);
%             c = [c0; epsilon_mag(i)*ones(2,1)];
%             k = length(c);
%             [xopt, uopt, popt] = getElasticaGivenP0(x0, p0, c);
%             Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_elastica, k)
%             xf = xopt(1);
%             xf(3) = angle_diff(xf(3),0);
%             tguess = linspace(t0,tf,50)';
%             xguess = xopt(tguess)';
%             uguess = uopt(tguess)';
%             [xopt2, uopt2, popt2, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
%             Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_elastica, k)
%             x0list{1} = x0;
%             xflist{1} = xf;
%             xoptlist{1} = xopt;
%             uoptlist{1} = uopt;
%             Voptlist{1} = Vopt(1:5);
%             for j=1:M
%                 costopt{j} = c(1:5)'*Voptlist{j};
%             end
%             fprintf('error between gpops and true: %e\n',norm(Vopt - Vopt2));
%             if norm(Vopt-Vopt2) > 1
%                 continue
%             end
%             out = Direct_IOC_elastica(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
%             out1 = MaxMargin_IOC_elastica(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
%             out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c(1:5), xopt, uopt, Vopt(1:5), costopt{1}, tguess, xguess, uguess, timestamp);
%             out4 = KKT_IOC_elastica( t0, tf, x0list, xflist, c(1:5), xoptlist, uoptlist, Voptlist, costopt, tguess, xguess, uguess, timestamp);
%             i = i+1
%         end
%     end
%     
% end
% 
% 
% %% Unicycle %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');  % initialize random number generator
    n = 3;
    m = 2;
    k = n+m;
    Nexp = 20;
    for i=1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        t0 = 0;
        tf = 8;
        x0 = [0, -2, -pi]' + [2, 4, 2*pi]'.*rand(3,1);
        xf = nan;
        c = [0.1; 10.^(-2*rand(k-1,1))]
        tguess = [t0; tf];
        xguess = [x0'; [0,0,0]];
        uguess = [[0 0]; [0 0]];
        [xopt, uopt, ~,~,~,~] = unicyclegpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
        Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_unicycle, k);
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt;
        uoptlist{1} = uopt;
        Voptlist{1} = Vopt;
        tguess = linspace(t0,tf,50)';
        xguess = xopt(tguess)';
        uguess = uopt(tguess)';
%         out = Direct_IOC_unicycle(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
%         out1 = MaxMargin_IOC_unicycle(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
%         out3 = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp);
        out4 = KKT_IOC_unicycle( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, tguess, xguess, uguess, timestamp);
    end
end
%% Perturbation Unicycle
if 0,
    clear global variables
    seed = 1;
    rngstate = rng(seed,'twister');  % initialize random number generator
    n = 3;
    m = 2;
    k = n+m;
    
    t0 = 0;
    tf = 8;
    x0 = [0, -2, -pi]' + [2, 4, 2*pi]'.*rand(3,1);
    xf = nan;
    c = [0.1; 10.^(-2*rand(k-1,1))]
    tguess = [t0; tf];
    xguess = [x0'; [0,0,0]];
    uguess = [[0 0]; [0 0]];
    [xopt, uopt, ~,~,~,~] = unicyclegpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
    Vopt = FeatureExpectations(xopt, uopt, t0, tf, @phi_unicycle, k);
    tguess = linspace(t0,tf,50)';
    xguess = xopt(tguess)';
    uguess = uopt(tguess)';
        
    epsilon_mag = 10.^linspace(-5,-1,20);
    
    for i = 1:length(epsilon_mag), %1:Nexp,
        timestamp = datestr(now,'yyyymmdd-HHMMSS');
        
        k2 = 1; % kk = 1 + 2*k*(n+m)
        epsilon = zeros(1 + 2*k2*(n+m),1);
        epsilon(2) = epsilon_mag(i);
        
        [xopt2, uopt2, popt2, ~, ~, ~] = unicyclegpopsPerturbMain(t0, tf, x0, xf, c, epsilon, tguess, xguess, uguess);
        Vopt2 = FeatureExpectations(xopt2, uopt2, t0, tf, @phi_unicycle, k)
        x0list{1} = x0;
        xflist{1} = xf;
        xoptlist{1} = xopt2;
        uoptlist{1} = uopt2;
        Voptlist{1} = Vopt2;
        tguess = linspace(t0,tf,50)';
        xguess = xopt2(tguess)';
        uguess = uopt2(tguess)';
        out = Direct_IOC_unicycle(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out1 = MaxMargin_IOC_unicycle(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out3 = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, xopt2, uopt2, Vopt2, tguess, xguess, uguess, timestamp);
        out4 = KKT_IOC_unicycle( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, tguess, xguess, uguess, timestamp);
    end
end




% 
% %% Locomotion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if 1,
%     % state x: (x,y,theta, vforw, vside, omega)
%     % input u: (u1, u2, u3)   forward, sideward, and rotational accels
%     t0 = 0;
%     tf = 3;
%     x0 = [0,0,pi/2,0,0,0]';
%     timestamp = datestr(now,'yyyymmdd-HHMMSS');
%     %xf = [-2, 1, 3*pi/4, 0, 0, 0]';  % Mombaur's target 2
%     xf = [3,1,pi/4,0,0,0]';  % Mombaur's target 1
%     %xf = [3, -1, -pi/4, 0, 0, 0]';
%     %c = [1, 1.2, 1.7, 0.7, 5.2]'; % Mombaur's final "good" values
%     %c = [1.0, 1.4, 0.58, 4.3]';
%     c = [1.0, 1.4, 0.58]';
%     tguess = [t0; tf];
%     xguess = [x0'; xf'];
%     uguess = [[0,0,0]; [0,0,0]];
%     tic
%     [xopt, uopt, ~, ~, ~, ~,tfopt] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
%     toc
%     Vopt = FeatureExpectations(xopt,uopt,t0,tfopt,@phi_locomotion, xf);
%     %         tguess = linspace(t0,tf,50)';
%     %         xguess = xopt(tguess)';
%     %         uguess = uopt(tguess)';
%     %         tf = nan;
%     %         [xopt2, uopt2, ~, ~, ~, ~,tfopt2] = locomotiongpopsMain(t0, tf, x0, xf, c, tguess, xguess, uguess);
%     %tguess = linspace(0,tfopt,50)';
%     %%xguess = zeros(length(tguess), 7);
%     %%xguess(:,2) = linspace(0,xf(1),50)';
%     %%ugues = zeros(length(tguess), 3);
%     %xguess = xopt(tguess)';
%     %uguess = uopt(tguess)';
%     %Mx = [tguess, xguess, zeros(length(tguess),1)];
%     %Mu = [tguess, uguess];
%     %tic
%     %[x1, u1, ~, ~, ~, ~, tfopt2] = locomotionacadoMain(t0, tf, x0, xf, c, Mx, Mu);
%     %toc
%     x0list{1} = x0;
%     xflist{1} = xf;
%     Voptlist{1} = Vopt;
%     xoptlist{1} = xopt;
%     uoptlist{1} = uopt;
%     %out = KKT_IOC_locomotion(t0, tfopt, x0list, xflist, c, xoptlist, uoptlist, Voptlist, timestamp);
%     %out = Direct_IOC_locomotion(t0, tf, x0, xf, c, xopt, uopt, tfopt, Vopt, tguess, xguess, uguess, timestamp);
%     %out1 = MaxMargin_IOC_locomotion(t0, tf, x0, xf, c, timestamp);
%     %out1a = MaxMargin_IOC_elastica_a(t0, tf, x0, xf, c, timestamp);
%     %out2 = MaxMargin_IOC_elastica2(t0, tf, x0, xf, c, timestamp);
%     %out3 = MaxMargin_IOC_elastica3(t0, tf, x0, xf, c, timestamp);
% end
% 
% 
end