function [ out ] = MaxMargin_IOC_unicycle3(t0, tf, x0, xf, c, xopt, uopt, Vopt, tguess, xguess, uguess, timestamp)
% out = MaxMargin_IOC()
%

% number of cost basis functions
k = length(c);  
costopt = c'*Vopt;


%% Solve the inverse optimal control problem for unknown cost function parameters.
% initial guess for unknown parms
chat = 0.1*ones(size(c));            


%% begin iterations.
tic;
chist = {};
xhist = {};
uhist = {};
phist = {};
feathist = {};
forwardtimes = {};
costhist = {};
lambda = 0.1;
alpha = 1/(10*lambda);
%r = 1;
epsilon = 1e-5; % convergence criterion
run = 1; 
i = 1;
error_old = inf;
max_iterations = inf;
delta_error = inf;
slow_count = 0;
while run,
    % solve forward problem at current value of parameters, chat
    forwardtstart = tic;
    [xi, ui, pi, ~, ~, ~] = unicyclegpopsMain(t0, tf, x0, xf, chat, tguess, xguess, uguess);
    forwardtimes{i} = toc(forwardtstart);
    V = FeatureExpectations( xi, ui, t0, tf, @phi_unicycle, k )
    
    %alpha = r/i;
    chat_new = chat - alpha * (Vopt - V + lambda*chat);  % iterative update step
    chat_new = project(chat_new); % project chat into desired convex set
    %error = norm(chat_new - chat) % check convergence of parameters.
    error = norm(V - Vopt)^2/norm(Vopt)^2;
    
    delta_error = abs(error - error_old);
    if (error >= error_old)
        alpha = 0.7*alpha;
        if alpha < 1e-6
            break;
        end
    end
    
    costhist{i} = chat'*V;
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    feathist{i} = V;
    chist{i} = chat;
    alphahist{i} = alpha;
    
    fprintf('alpha = %e\n',alpha);
    fprintf('costerror = %e\n', norm(costopt - costhist{end}));
    fprintf('featerror = %e\n',norm(Vopt - feathist{end}));
    try
        fprintf('delta feat = %e\n',norm(feathist{end} - feathist{end-1}));
    catch
    end
    fprintf('parmerror = %e\n',norm(c - chist{end}));
    
    chat = chat_new;
    
    if (delta_error < epsilon/10)
        if (slow_count > 5)
            break;
        else
            slow_count = slow_count + 1
        end
    else
        slow_count = 0;
    end
    error_old = error;
    run = error > epsilon; % keep running until we satisfy convergence criterion
    
    if length(chist) > 1
        if norm(chist{end} - chist{end-1}) < 1e-4
            run = false;
        end
    end
    if i > max_iterations,
        break;
    end
    i = i+1
end

totalTime = toc;

out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.costopt = costopt;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.whist = {};
out.chist = chist;
out.feathist = feathist;
out.xhist = xhist;
out.uhist = uhist;
out.phist = phist;
out.forwardtimes = forwardtimes;
out.costhist = costhist;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-max-margin3-unicycle.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-max-margin3-unicycle.mat'];
end
save(filename, 'out');

end

function cproj = project(c)

%cproj = zeros(size(c));
cproj = c;

% project to interval [0.01, 1]^k
% first, normalize first element to 0.1
cproj = 0.1*(cproj/cproj(1));
% then, chop to fit bounds
for i = 2:length(c)
    cproj(i) = max(0.01,c(i));
    cproj(i) = min(1.0, cproj(i));
end

end

