function [x_traj, xopt, uopt, popt] = Forward_Problem(t0, tf, x0, xf, c)

% x_traj = [5*c^3; 7*c^2; 3];

[xopt, uopt, popt, ~, ~, ~] = elasticagpopsMain(t0, tf, x0, xf, c);
T = linspace(t0, tf, 50);
x_traj = [reshape(xopt(T)', 3*length(T), 1) ; uopt(T)'];
%x_traj = [xopt(:,1); xopt(:,2); xopt(:,3); uopt];


end