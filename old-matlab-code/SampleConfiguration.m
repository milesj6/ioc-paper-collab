function conf = SampleConfiguration(abnd)
while (1)
    a = abnd.*(-1+2*rand(6,1));
    conf = GetConfiguration(a);
    if (~conf.infeasible)
        return;
    end
end
