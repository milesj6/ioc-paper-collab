function [ out ] = Direct_IOC_locomotion(t0, tf, x0, xf, c, xopt, uopt, tfopt, Vopt, tguess, xguess, uguess, timestamp)
% out = Direct_IOC_locomotion(t0, tf, x0, xf, c, timestamp)

k = length(c);  % number of cost basis functions
n = length(x0);
m = length(uopt(0));

Topt = linspace(t0, tfopt, 50);
xuopt = [reshape(xopt(Topt)', n*length(Topt), 1) ; reshape(uopt(Topt)', m*length(Topt), 1)]; % stack of all x's and u's
costopt = c'*Vopt;

%% Solve the inverse optimal control problem for unknown cost function parameters.
c0 = ones(size(c)); %zeros(size(c));  % 
c0(1) = 1;            % initial guess for unknown parms

%% Some shared variables to store history of algorithm
i = 1;
chist = {};
xhist = {};
uhist = {};
feathist = {};
costhist = {};
objectivehist = {};
forwardtimes = {};


options = optimset('TolFun',1e-3,'TolX',1e-3);  % Note TolFun is tolerance on the change in objective fun
LB = zeros(length(c0)-1,1);
LB(1:4) = 0.01;
UB = 6 * ones(length(c0)-1,1);
%% solve.
tic;
[chat,fval,exitflag, output] = fminsearchbnd(@(c_remainder)objective(c_remainder,t0,tf,x0,xf,xuopt),c0(2:end),LB,UB,options);
%[chat, fval, exitflag, output, fminlambda, grad, hessian] = fmincon( @(c)objective(c,t0,tf,x0,xf,xopt,uopt,k), c0, A, b, [],[],[],[],[],options)
totalTime = toc;

out.totaTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0;
out.xf = xf;
out.c = c;
out.xopt = xopt;
out.uopt = uopt;
out.Vopt = Vopt;
out.costopt = costopt;
out.chat = chat;
out.whist = {};
out.xhist = xhist;
out.uhist = uhist;
out.feathist = feathist;
out.costhist = costhist;
out.chist = chist;
out.objectivehist = objectivehist;
out.forwardtimes = forwardtimes;
out.fval = fval;
out.exitflag = exitflag;
out.fminoutput = output;


outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-direct-locomotion.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-direct-locomotion.mat'];
end
save(filename, 'out');



    function [ output ] = objective( c_remainder, t0, tf, x0, xf, xuopt )
        %output = maxmargin_locomotion2_objective( c )
        %
        %   c: variables of optimization = weights in forward problem cost function
        %
        
        ci = [1; c_remainder]
        
        tic
        forwardstart = tic;
        [xi, ui, ~, ~, ~, ~, tfi] = locomotiongpopsMain(t0, tf, x0, xf, ci, tguess, xguess, uguess);
        forwardtimes{i} = toc(forwardstart)
        toc
        T = linspace(t0, tfi, 50);
        xu_predicted = [reshape(xi(T)', n*length(T), 1) ; reshape(ui(T)',m*length(T),1)];
        feathist{i} = FeatureExpectations(xi, ui, t0, tfi, @phi_locomotion, xf);
        output = sum((xuopt - xu_predicted).^2)

        chist{i} = ci;
        xhist{i} = xi;
        uhist{i} = ui;
        costhist{i} = ci'*feathist{i};
        objectivehist{i} = output;
        i = i + 1
    end

end