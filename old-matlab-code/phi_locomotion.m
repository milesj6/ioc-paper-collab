function [ vec ] = phi_locomotion( t, x, u, xf )
%
% t: N x 1
% x: n x N 
% u: m x N

if strcmp(class(t),'ad')
    tsamp = t.value;
else
    tsamp = t;
end
if strcmp(class(x),'ad')
    xsamp = x.value(:,:);
elseif strcmp(class(x),'function_handle')
    xsamp = x(tsamp);
else
    xsamp = x;
end
if strcmp(class(u),'ad')
    usamp = u.value(:,:);
elseif strcmp(class(u),'function_handle')
    usamp = u(tsamp);
else
    usamp = u;
end

N = length(tsamp);
vec = zeros(4,N);

for i=1:N
    %vec(:,i) = vertcat(xsamp(:,i).^2, usamp(:,i).^2);
    %vec(1,i) = 1;
    vec(1,i) = usamp(1,i)^2;
    vec(2,i) = usamp(2,i)^2;
    vec(3,i) = usamp(3,i)^2;
    vec(4,i) = angle_diff(atan2(xf(2) - xsamp(2,i),  xf(1) - xsamp(1,i)) - xsamp(3,i), 0)^2;
end


end

