function [xopt, uopt, popt, xoptp, uoptp, poptp] = unicyclegpopsPerturbMain(t0, tf, x0, xf, cin, epsilonin, tguess, xguess, uguess)

clear global setup limits guess;

%%%%%%%%%% set up global variables %%%%%%%%%
global c k epsilon k2
k = length(cin);  % number of basis functions = n + m
c = cin;
epsilon = epsilonin;
n = length(x0);
m = length(uguess(1,:));
k2 = (length(epsilon) - 1) / (2*(n+m)); % kk = 1 + 2*k*(n+m);
%%%%%%%%%% end global variables %%%%%%%%%

disp('Solve forward optimal control problem...');
disp('');

n = 3; % state vector dimension
m = 2; % input vector dimension

xmin = [-5 -5 -pi]';
xmax = [5 5 pi]';
if any(isnan(xf)),
    xfmin = xmin;
    xfmax = xmax;
    xf = [0,0,0]';
else
    xfmin = xf;
    xfmax = xf;
end
umin = [-30 -30]';
umax = [30 30]';

iphase = 1;  % only one "phase"

%%%%%%%%% Set up limits %%%%%%%%%%%%%%%
%limits(iphase).meshPoints = -1:0.2:1;
%limits(iphase).nodesPerInterval = 10*ones(1,length(limits(iphase).meshPoints)-1);
limits(iphase).time.min = [t0 tf];
limits(iphase).time.max = [t0 tf];
limits(iphase).state.min = [x0 xmin xfmin];
limits(iphase).state.max = [x0 xmax xfmax];
limits(iphase).control.min = umin;
limits(iphase).control.max = umax;
limits(iphase).parameter.min = [];
limits(iphase).parameter.max = [];
limits(iphase).path.min = [];
limits(iphase).path.max = [];
limits(iphase).event.min = [];
limits(iphase).event.max = [];

%%%%%%%% Set up guesses %%%%%%%%%%%%%%
%tguess = linspace(0,1,50);
%xguess_samp = xguess(tguess);
%for i=1:length(tguess)
%    uguess_samp(i) = uguess(tguess(i));
%end
%uguess_samp = uguess(tguess);
%guess(iphase).time = [t0; tf];
%guess(iphase).state = [x0'; xf'];
%guess(iphase).control = [[0 0]; [0 0]];
guess(iphase).time = tguess; %[t0; tf];
guess(iphase).state = xguess; %[x0'; xf'];
guess(iphase).control = uguess; %[0; 0];
guess(iphase).parameter = [];

setup.name = 'unicycle';
setup.funcs.cost = 'unicyclegpopsPerturbCost';
setup.funcs.dae = 'unicyclegpopsDAE';
setup.printoff = 1;
setup.limits = limits;
setup.derivatives = 'analytic';
%setup.derivatives = 'finite-difference';
%setup.derivatives = 'automatic';
setup.checkDerivatives = 0;
setup.guess = guess;
setup.linkages = [];
setup.autoscale = 'off';
setup.tolerances = [1e-8,1e-8];
setup.mesh.tolerance = 1e-3;
%setup.mesh.iteration = 0;
setup.mesh.iteration = 10;
setup.mesh.nodesPerInterval.min = 10;
setup.mesh.nodesPerInterval.max = 100;

[output, gpopsHistory] = gpops(setup);
solution = output.solution;
solutionPlot = output.solutionPlot;

topt = solutionPlot.time;
xoptsamp = solutionPlot.state;
uoptsamp = solutionPlot.control;
poptsamp = solutionPlot.costate;

% interpolate x, and u to get obtain state and input as functions of time
xtmp = csaps(topt, xoptsamp', 1);
utmp = csaps(topt, uoptsamp', 1);
ptmp = csaps(topt, poptsamp', 1);
xopt = @(t) fnval(xtmp,t);
uopt = @(t) fnval(utmp,t);
popt = @(t) fnval(ptmp,t);

xtmpprime = fnder(xtmp);
utmpprime = fnder(utmp);
ptmpprime = fnder(ptmp);
xoptp = @(t) fnval(xtmpprime,t);
uoptp = @(t) fnval(utmpprime,t);
poptp = @(t) fnval(ptmpprime,t);

disp('done...');
end