function [ dae, deriv ] = lqrgpopsDAE( sol )
global Asys Bsys

t = sol.time; % N x 1 col vector
x = sol.state; % N x n matrix
u = sol.control; % N x m matrix
p = sol.parameter;

N = length(t);
n = size(x,2);

%dae = [u(:,1).*cos(x(:,3)) u(:,1).*sin(x(:,3)) u(:,2)];
dae = zeros(N, n);

for i = 1:N,
    dae(i,:) = (Asys * x(i,:)' + Bsys * u(i,:)')';
end


% df1_dx1 = zeros(size(t));
% df1_dx2 = zeros(size(t));
% df1_dx3 = -u(:,1).*sin(x(:,3));
% df1_du1 = cos(x(:,3));
% df1_du2 = zeros(size(t));
% df2_dx1 = zeros(size(t));
% df2_dx2 = zeros(size(t));
% df2_dx3 = u(:,1).*cos(x(:,3));
% df2_du1 = sin(x(:,3));
% df2_du2 = zeros(size(t));
% df3_dx1 = zeros(size(t));
% df3_dx2 = zeros(size(t));
% df3_dx3 = zeros(size(t));
% df3_du1 = zeros(size(t));
% df3_du2 = ones(size(t));

df1_dx1 = repmat(Asys(1,1), N, 1);
df1_dx2 = repmat(Asys(1,2), N, 1); %zeros(size(t));
df1_dx3 = repmat(Asys(1,3), N, 1); %-u(:,1).*sin(x(:,3));
df1_du1 = repmat(Bsys(1,1), N, 1); %cos(x(:,3));
df1_du2 = repmat(Bsys(1,2), N, 1); %zeros(size(t));
df2_dx1 = repmat(Asys(2,1), N, 1); %zeros(size(t));
df2_dx2 = repmat(Asys(2,2), N, 1); %zeros(size(t));
df2_dx3 = repmat(Asys(2,3), N, 1); %u(:,1).*cos(x(:,3));
df2_du1 = repmat(Bsys(2,1), N, 1); %sin(x(:,3));
df2_du2 = repmat(Bsys(2,2), N, 1); %zeros(size(t));
df3_dx1 = repmat(Asys(3,1), N, 1); %zeros(size(t));
df3_dx2 = repmat(Asys(3,2), N, 1); %zeros(size(t));
df3_dx3 = repmat(Asys(3,3), N, 1); %zeros(size(t));
df3_du1 = repmat(Bsys(3,1), N, 1); %zeros(size(t));
df3_du2 = repmat(Bsys(3,2), N, 1); %ones(size(t));

df1_dp = zeros(length(t),length(p));
df1_dt = zeros(size(t));
df2_dp = zeros(length(t),length(p));
df2_dt = zeros(size(t));
df3_dp = zeros(length(t),length(p));
df3_dt = zeros(size(t));

deriv = [df1_dx1, df1_dx2, df1_dx3, df1_du1, df1_du2, df1_dt, df1_dp; ...
         df2_dx1, df2_dx2, df2_dx3, df2_du1, df2_du2, df2_dt, df2_dp; ...
         df3_dx1, df3_dx2, df3_dx3, df3_du1, df3_du2, df3_dt, df3_dp];

end

