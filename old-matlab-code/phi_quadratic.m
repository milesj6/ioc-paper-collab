function [ vec ] = phi_quadratic( t, x, u, varargin )
% %
% % t: N x 1
% % x: n x N 
% % u: m x N
% 
% vec = vertcat(x(t).^2, u(t).^2);

%
% t: N x 1
% x: n x N 
% u: m x N

if strcmp(class(t),'ad')
    tsamp = t.value;
else
    tsamp = t;
end
if strcmp(class(x),'ad')
    xsamp = x.value(:,:);
elseif strcmp(class(x),'function_handle')
    xsamp = x(tsamp);
else
    xsamp = x;
end
if strcmp(class(u),'ad')
    usamp = u.value(:,:);
elseif strcmp(class(u),'function_handle')
    usamp = u(tsamp);
else
    usamp = u;
end

n = size(xsamp,1);
m = size(usamp,1);
k = n+m;
N = length(tsamp);
vec = zeros(k,N);

for i=1:N
    vec(:,i) = vertcat(xsamp(:,i).^2, usamp(:,i).^2);
end

end

