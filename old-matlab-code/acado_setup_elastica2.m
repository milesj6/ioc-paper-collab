clear;

BEGIN_ACADO;

    %% Initialize ACADO parameters
    acadoSet('problemname','acado_elastica2');
    
    DifferentialState x1 x2 x3 L;
    
    Control u;
    
    Parameter p1 p2 p3 p4 p5;
    
    c = acado.MexInputVector;

    p1 = c(1);
    p2 = c(2);
    p3 = c(3);
    p4 = c(4);
    p5 = c(5);
    
    xf1 = acado.MexInput;
    xf2 = acado.MexInput;
    xf3 = acado.MexInput;

    %% Equations of motion
    f = acado.DifferentialEquation();
    f.linkMatlabODE('acadoelasticaode');
    
    %% Optimal Control Problem (OCP)
    t0 = 0;
    tf = 1;
    N = 50;
    ocp = acado.OCP(t0, tf, N);
    
    %% Objective
    ocp.minimizeLagrangeTerm( L );
    %% Constraints
    ocp.subjectTo( f );
    ocp.subjectTo( 'AT_START', x1 == 0 );
    ocp.subjectTo( 'AT_START', x2 == 0 );
    ocp.subjectTo( 'AT_START', x3 == 0 );
    ocp.subjectTo( 'AT_START', L == 0 );
    ocp.subjectTo( 'AT_END', x1 == xf1 );
    ocp.subjectTo( 'AT_END', x2 == xf2 );
    ocp.subjectTo( 'AT_END', x3 == xf3 );
    ocp.subjectTo( -50 <= u <= 50 );
    
    algo = acado.OptimizationAlgorithm(ocp);
    algo.set('DISCRETIZATION_TYPE','MULTIPLE_SHOOTING');
    
    %% Initial Guess
    tsamp = linspace(0,1,50)';
    xsamp = zeros(length(tsamp), 4);
    xsamp(:,2) = linspace(0,1,50)';
    M = [tsamp, xsamp];
    algo.initializeDifferentialStates(M);
    
    usamp = zeros(length(tsamp), 1);
    M = [tsamp, usamp];
    algo.initializeControls(M);
    
END_ACADO;

% tic
% c = [1.0, 0, 0, 0, 0]';
% xf = [0.1687, -0.6698, 0.4209]';
% out = elastica_RUN(c(1),c(2),c(3),c(4),c(5), xf(1), xf(2), xf(3));
% toc