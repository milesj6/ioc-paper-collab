function plotMaxMarginData(filepattern)

close all

currentFolder = pwd;
ignore = userpath;
ignore = ignore(1:end-1);
cd(ignore);


% Get all saved data 
%filenames = dir('*max-margin*.mat');
filenames = dir(filepattern);
N = size(filenames,1);

% For each data file, plot results
%for filename = {filenames(:).name},
for i = 1:N,
    disp(sprintf('filename = %s',filenames(i).name));
    getDataStats(filenames(i).name);
end

cd(currentFolder);

end

function getDataStats(filename)
    [pathstr, name, ext] = fileparts(filename);
    
    % make sure ioc code is in path:
    %curpath = path;
    %newpath = path('~/Dropbox/ioc/code/',path);

    % load data from .mat file
    datatmp = load(filename);
    data = datatmp.out;
    
    
    
    % normalize cost parameters:
    for i=1:length(data.chist)
        data.chist{i} = (1/data.chist{i}(1)) * data.chist{i};
    end
    
    try
        disp('totaltime = ')
        disp(data.totalTime)
    catch err
        disp('totalTime field not found');
    end
    
    disp('parameter error = ')
    disp(norm(data.chist{end} - data.c));
    
    try
    disp('cost error = ')
    disp(norm( data.chist{end}'*data.feathist{end} - data.c'*data.Vopt) );
    catch
        disp('cost end = ')
    disp(data.costhist{end});
    end
%     t = linspace(0,1,100);
%     xsamp = data.xopt(t);
%     usamp = data.uopt(t);
% 
%     h = figure();
%     hold on
%     axis equal
%     %% Plot optimal trajectory and trajectory iterates
%     plot(xsamp(1,:), xsamp(2,:), 'r-','Linewidth',2);
%     NN = length(data.xhist);
%     for i=1:NN,
%         xi = data.xhist{i}(t);
%         plot(xi(1,:), xi(2,:), 'b-');
%     end
%     plot(xi(1,:), xi(2,:),'go');
%     xlabel('x1');
%     ylabel('x2');
%     title('Trajectory, x1 x2 plane');
%     filename = [name,'-traj.eps'];
%     saveas(h, filename, 'epsc2');
%     
%     %% Plot control inputs
%     h2 = figure();
%     hold on
%     plot(t,usamp, 'r-','Linewidth',2);
%     for i=1:NN,
%         ui = data.uhist{i}(t);
%         plot(t,ui,'b-');
%     end
%     plot(t,ui,'go');
%     xlabel('time');
%     ylabel('u(t)');
%     title('control input');
%     filename = [name,'-input.eps'];
%     saveas(h2, filename, 'epsc2');
%     
%     %% Plot iterates of cost parameters.
%     NN = size([data.chist{:}],2)
%     h3 = figure();
%     plot(sqrt(sum(([data.chist{:}]-repmat(data.c,1,NN)).^2)));
%     xlabel('iteration');
%     ylabel('norm(ci - c)');
%     title('parameter error over iterations');
%     filename = [name,'-parmerror.eps'];
%     saveas(h3, filename, 'epsc2');
%     
%     %% Plot iterates of margin
%     h4 = figure();
%     disp('data.whist = ')
%     disp([data.whist{2:end}]);
%     plot(abs([data.whist{2:end}]));
%     xlabel('iteration');
%     ylabel('margin, w');
%     title('margin over iterations');
%     filename = [name, '-margin.eps'];
%     saveas(h4, filename, 'epsc2');
%     
%     %% Plot distance between feature expectations and demonstration feature expectations.
%     h5 = figure();
%     dfeat = []
%     for i = 1:length(data.feathist),
%         dfeat(i) = norm(data.Vopt - data.feathist{i});
%     end
%     plot(dfeat,'bo');
%     xlabel('iterations i');
%     ylabel('feature error');
%     title('feature error over iterations');
%     filename = [name,'-featerror.eps'];
%     saveas(h5, filename, 'epsc2');
%     
%     %% Plot improvement beteen iterations.
%     h6 = figure();
%     deltaw = [data.whist{2:end}] - [data.whist{1:end-1}];
%     plot(deltaw,'bo');
%     xlabel('iterations i');
%     ylabel('margin improvement');
%     title('margin improvement over iterations');
%     filename = [name,'-marginimprov.eps'];
%     saveas(h6, filename, 'epsc2');
%     
%     %path(path);
end