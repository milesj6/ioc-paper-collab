

BEGIN_ACADO;

    %% Initialize ACADO parameters
    acadoSet('problemname','acado_locomotion');
    
    DifferentialState x1 x2 x3 x4 x5 x6 L;
    Control u1 u2 u3;
    
    %Parameter p1 p2 p3 p4 p5 p6 p7 p8 T;
    Parameter T;
    
%     % cost function parameters
%     c1 = acado.MexInput;
%     c2 = acado.MexInput;
%     c3 = acado.MexInput;
%     c4 = acado.MexInput;
%     c5 = acado.MexInput;
    
    % terminal state
    xf1 = acado.MexInput;
    xf2 = acado.MexInput;
    xf3 = acado.MexInput;
    xf4 = 0.0;
    xf5 = 0.0;
    xf6 = 0.0;
    
%     p1 = c1;
%     p2 = c2;
%     p3 = c3;
%     p4 = c4;
%     p5 = c5;
%     p6 = xf1;
%     p7 = xf2;
%     p8 = xf3;
    
    % initial guess for state and control
    Mx = acado.MexInputMatrix;
    Mu = acado.MexInputMatrix;

    %% Equations of motion  (x,y,theta, vforw, omega, vorth)
    f = acado.DifferentialEquation();
    f.linkMatlabODE('acadolocomotionode');
%     f = acado.DifferentialEquation(0.0, T);
%     f.add(dot(x1) == x4*cos(x3) - x6*sin(x3));
%     f.add(dot(x2) == x4*sin(x3) - x6*cos(x3));
%     f.add(dot(x3) == x5);
%     f.add(dot(x4) == u1);
%     f.add(dot(x5) == u2);
%     f.add(dot(x6) == u3);
    
    %% Optimal Control Problem (OCP)
    t0 = 0.0;
    % tf = 1;
    N = 50;
    ocp = acado.OCP(t0, T, N);
    
    %% Objective
    ocp.minimizeMayerTerm( L );
    
    %% Constraints
    ocp.subjectTo( f );
    ocp.subjectTo( 'AT_START', x1 == 0.0 );
    ocp.subjectTo( 'AT_START', x2 == 0.0 );
    ocp.subjectTo( 'AT_START', x3 == 0.0 );
    ocp.subjectTo( 'AT_START', x4 == 0.0 );
    ocp.subjectTo( 'AT_START', x5 == 0.0 );
    ocp.subjectTo( 'AT_START', x6 == 0.0 );
    ocp.subjectTo( 'AT_START', L == 0.0 );
    ocp.subjectTo( 'AT_END', x1 == xf1 );
    ocp.subjectTo( 'AT_END', x2 == xf2 );
    ocp.subjectTo( 'AT_END', x3 == xf3 );
    ocp.subjectTo( 'AT_END', x4 == xf4 );
    ocp.subjectTo( 'AT_END', x5 == xf5 );
    ocp.subjectTo( 'AT_END', x6 == xf6 );
    ocp.subjectTo( -20.0 <= u1 <= 20.0 );
    ocp.subjectTo( -20.0 <= u2 <= 20.0 );
    ocp.subjectTo( -20.0 <= u3 <= 20.0 );
    ocp.subjectTo( 3.0 <= T <= 7.0 );
    
    algo = acado.OptimizationAlgorithm(ocp);
    algo.set('DISCRETIZATION_TYPE','MULTIPLE_SHOOTING');
    algo.set('KKT_TOLERANCE',1e-8);
    algo.set('INTEGRATOR_TOLERANCE',1e-10);
    algo.set('ABSOLUTE_TOLERANCE',1e-10);
    
    %% Initial Guess
    algo.initializeDifferentialStates(Mx);
    algo.initializeControls(Mu);
    
END_ACADO;

% tic
% c = [1.0, 0, 0, 0, 0]';
% xf = [0.1687, -0.6698, 0.4209]';
% out = elastica_RUN(c(1),c(2),c(3),c(4),c(5), xf(1), xf(2), xf(3));
% toc