function [ chist, whist, xhist, uhist, phist, feathist ] = maxmargin( t0, tf, x0, c0, xopt, uopt)
%   [chist, whist, feathist] = maxmargin(c0, D0, Dopt)
%
%   Inputs:
%       x0: initial state
%       xf: goal state
%       c0: initial guess for cost function parameters.
%       Dopt: the given optimal trajectory (i.e. the expert observation).
%
%   Return value:
%       chist: The history of iterates, c_i
%       whist: The history of margins w_i
%       feathist: history of feature expectations 
%

n=3;
m=2;

i = 1; % counter to keep track of number of iterations

% optimal trajectory (observation) feature expectations.
Vopt = FeatureExpectations(xopt, uopt, t0, tf);

% Solve forward problem for initial cost function guess.
[x1, u1, p1, x1p, u1p, p1p] = unicyclegpopsMain(t0, tf, x0, c0);
feathist{i} = FeatureExpectations(x1, u1, t0, tf);


% Initialize some arrays that we will be using.
xhist{i} = x1; % optimal trajectory iterates
uhist{i} = u1; % optimal control iterates
phist{i} = p1; % optimal costate iterates



chist{i} = [];
whist{i} = 1;
epsilon = 1e-4;  % threshold for terminating iterations.
%N = size(A,2);

% Linear inequality constrains: A c <= b
A = [];
b = [];

% Constrain the parameters to be positive: A2 c <= b
% Note that we constrain the control cost to be bounded away from zero 
% to avoid numerical issues with forward problem solver.
% This would all be handled more gracefully if we use unconstrained
% parameters, and then project them onto a desired parameter subspace
% (Ratliff et al.).
A2 = -eye(n+m); 
A2(n+m+1,n+m+1) = 0;
b2 = [0,0,0,-0.01,-0.01,0]';  


% Constrain the first cost function parameter to be equal to "1".  
% This is our standard normalization step.
Aeq = zeros(1,length(c0)+1);
Aeq(1) = 1.0;
beq = 1;

i = 2; % we've already initialized stuff
imax = 50;

% initial guess of unknown parms.  note that we append a guess of zero
% for the unknown "margin" for the fmincon optimization problem.
c0hat = [c0; 0]; 
while(abs(whist{end}) > epsilon),
    % list of linear inequality constraints: A x <= b  where "x" is the
    % optimization variable, in our case "x" = c.
    A = [A, [Vopt - feathist{end}; -1]];  % append new linear inequality constraint
    b = zeros(i-1,1); 
    %N = size(A,2); % number of columns is the number of constraints we have

    % Solve quadratic program (SVM problem).
    cstari = fmincon( @(c)c(end), c0hat, [A';A2], [b; b2], Aeq, beq, [], [], @maxmargin_nonlincon)
    
    whist{i} = cstari(end);
    chist{i} = cstari(1:end-1);

    % cut iterations off if convergence is taking too long
    if i > imax
        break;
    end
    
    % Solve forward problem given:  start and goal state, cost func parms c.
    [xi, ui, pi, xip, uip, pip] = unicyclegpopsMain(t0, tf, x0, cstari);
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    %feathist = [feathist, FeatureExpectations(xi, ui, t0, tf)];
    feathist{i} = FeatureExpectations(xi,ui,t0,tf);

    i = i+1;

end % end while loop


end

