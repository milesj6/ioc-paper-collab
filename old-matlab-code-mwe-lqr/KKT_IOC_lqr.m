function [ out ] = KKT_IOC_lqr( t0, tf, x0list, xflist, c, xoptlist, uoptlist, Voptlist, Asysin, Bsysin, timestamp)

global n m nn M k Asys Bsys

k = length(c)
n = length(x0list{1})
m = length(uoptlist{1}(0))
M = length(xoptlist)
nn = k + M*n;
Asys = Asysin;
Bsys = Bsysin;

for i=1:M
    costopt{i} = c'*Voptlist{i};
end

A = zeros(nn,nn);
B = zeros(nn, M*n);
B(k+1:end,:) = eye(M*n);

% Riccati solution
tic;
Pf = zeros(nn*nn,1);
options = odeset('RelTol',1e-8,'AbsTol',1e-8);
sol = ode45(@(t,x)rde(t, x, A, B, xoptlist, uoptlist), [tf, t0], Pf, options);
P = @(t)deval(sol,t);

% Minimize P(0) w.r.t. z(0) to find cost params and initial costates
P0 = reshape(P(0),nn,nn);
LB = 0.01*ones(k,1);
UB = ones(k,1);
Aeq = zeros(1,nn);
Aeq(1) = 1;
beq = 0.1;  % set c(1) := 0.1
options = optimset('Algorithm','interior-point-convex','TolFun',1e-14,'TolX',1e-14,'TolCon',1e-14);
[zhat, fval, exitflag, qpoutput, qplambda] = quadprog(P0, [], [], [], Aeq, beq,LB,UB,[],options);
totalTime = toc;
chat = zhat(1:k);
p0 = zhat(k+1:end);  % p0 for all trajectories!

%% Compute "predicted" trajectory from learned parameters
xhist = {};
uhist = {};
phist = {};
chist = {};
feathist = {};
for i = 1:M
    x0i = x0list{i};
    xfi = xflist{i};
    p0i = p0((i-1)*n + 1:i*n);
    [xi, ui, pi, ~, ~, ~] = lqrMain(t0, tf, x0i, xfi, chat, Asys, Bsys);
    Vi = FeatureExpectations(xi,ui,t0,tf,@phi_quadratic, xflist{i});
    costi = chat'*Vi;
    xhist{i} = xi;
    uhist{i} = ui;
    phist{i} = pi;
    chist{i} = chat;
    costhist{i} = costi;
    feathist{i} = Vi;
    
    fprintf('parm error = %e\n',norm(chat - c));
    fprintf('feat error = %e\n',norm(Vi - Voptlist{i}));
end

out.totalTime = totalTime;
out.t0 = t0;
out.tf = tf;
out.x0 = x0list;
out.xf = xflist;
out.c = c;
out.costopt = costopt;
out.xopt = xoptlist;
out.uopt = uoptlist;
out.Vopt = Voptlist;
out.chist = chist;
out.xhist = xhist;
out.uhist = uhist;
out.phist = phist;
out.feathist = feathist;
out.costhist = costhist;
out.P0 = P0;
out.zhat = zhat;
out.fval = fval;
out.chat = chat;
out.p0hat = p0;
out.exitflag = exitflag;
out.qpoutput = qpoutput;
out.qplambda = qplambda;

outdir = userpath;
outdir = outdir(1:end-1);
if timestamp,
    filename = [outdir filesep timestamp '-kkt-lqr.mat'];
else
    filename = [outdir filesep datestr(now,'yyyymmdd-HHMMSS') '-kkt-lqr.mat'];
end

save(filename, 'out');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Pdot = rde(t, x, A, B, xoptlist, uoptlist)
% Riccati Differential Equation
global nn

P = reshape(x,nn,nn);
Qt = Q(t, xoptlist, uoptlist);
Rt = R();
St = S(t, xoptlist, uoptlist);

Pdot = -A'*P - P*A + (P*B + St)*inv(Rt)*(P*B + St)' - Qt;
Pdot = reshape(Pdot, nn*nn, 1);

end

function fx = dfdx(t, xt, ut)
global Asys
fx = Asys;
end

function fu = dfdu(t, xt, ut)
global Bsys
fu = Bsys;
end

function phix = dphidx(t, xt, ut)
global k n
phix = zeros(k,n);
phix(1:n,1:n) = 2*diag(xt);
end

function phiu = dphidu(t, xt, ut)
global k n m
phiu = zeros(k,m);
phiu(n+1:k,:) = 2*diag(ut);
end

function Abar = computeAbar(t, xt, ut)
global n m k
Abar = zeros(n+m,k);
Abar(1:n,:) = dphidx(t,xt,ut)';
Abar(n+1:n+m,:) = dphidu(t,xt,ut)';
end

function Bbar = computeBbar(t, xt, ut)
global n m
Bbar = zeros(n+m,n);
Bbar(1:n,:) = dfdx(t, xt, ut)';
Bbar(n+1:n+m,:) = dfdu(t, xt, ut)';
end

function Cbar = computeCbar()
global n m
Cbar = zeros(n+m,n);
Cbar(1:n,:) = eye(n);
end

function F = computeF(t, xoptlist, uoptlist)
global n m k M
F = zeros(M*(n+m), k + M*n);
for i = 1:M
    xt = xoptlist{i}(t);
    ut = uoptlist{i}(t);
    i1 = (i-1)*(n+m)+1;   % row start index
    i2 = (i)*(n+m);       % row end   index
    j1 = k + 1 + (i-1)*n; % column start index
    j2 = k + 1 + (i)*n - 1;   % column end   index
    F(i1:i2, 1:k) = computeAbar(t, xt, ut);
    F(i1:i2, j1:j2) = computeBbar(t, xt, ut);
end
end

function G = computeG()
global n m M
G = zeros(M*(n+m), M*n);
for i = 1:M
    i1 = (i-1)*(n+m)+1;  % row start index
    i2 = (i) * (n+m);    % row   end index
    j1 = (i-1)*n + 1;    % col start index
    j2 = (i)*n;          % col   end index
    G(i1:i2, j1:j2) = computeCbar();
end
end

function Qout = Q(t, xoptlist, uoptlist)
Ft = computeF(t, xoptlist, uoptlist);
Qout = Ft'*Ft;
end

function Rout = R()
Gt = computeG();
Rout = Gt'*Gt;
end

function Sout = S(t, xoptlist, uoptlist)
Ft = computeF(t, xoptlist, uoptlist);
Gt = computeG();
Sout = Ft'*Gt;
end

